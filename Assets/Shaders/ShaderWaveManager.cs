using System.Collections;
using DG.Tweening;
using UnityEngine;

public class ShaderWaveManager : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject waveColliderPref;

    [SerializeField] private float waveTime = 0.75f;
    [SerializeField] private float waveMaxDistance = 1f;
    [SerializeField] private float waveFadeTime = 0.3f;

    private Coroutine waveCoroutine;
    private Material material;
    private Camera camera;
    private static int waveDistanceFromCenter = Shader.PropertyToID("_WaveDistanceFromCenter");
    private static int waveStrength = Shader.PropertyToID("_WaveStrength");
    private static int waveStartPosition = Shader.PropertyToID("_RingSpawnPos");

    private float newWaveDistance = -0.1f;
    private float newWaveStrength = -0.1f;
    private bool isWaving = false;

    private PlayerStateController playerStateController;

    private void Awake()
    {
        material = GetComponent<SpriteRenderer>().material;
        playerStateController = player.GetComponent<PlayerStateController>();
        camera = transform.GetComponentInParent<Camera>();
    }

    private void Update()
    {
        if (playerStateController.canUseIPD == false) 
        {
            return;
        }

        if (playerStateController != null && Input.GetKeyDown(playerStateController.ipdKey))
        {
            CallWave();
        }
        if (playerStateController != null && Input.GetKeyDown(playerStateController.ipdKeyAlt))
        {
            CallWave();
        }

        if (isWaving == true)
        {
            DOTween.To(()=> newWaveDistance, x=> newWaveDistance = x, waveMaxDistance, waveTime);
            DOTween.To(()=> newWaveStrength, x=> newWaveStrength = x, 0, waveTime + waveFadeTime).onComplete = ResetWaveDistance;
            material.SetFloat(waveDistanceFromCenter, newWaveDistance);
            material.SetFloat(waveStrength, newWaveStrength);
        }
    }

    public void CallWave()
    {
        isWaving = true;

        Vector2 startPosition = GetWaveStartPosition();
        material.SetVector(waveStartPosition, new Vector4(startPosition.x, startPosition.y));

        // Istanzia il collisore dell'onda e imposta il suo genitore
        var col = Instantiate(waveColliderPref, player.transform.position, Quaternion.identity);
        col.transform.SetParent(player.transform); // Imposta il wave collider come figlio del player
    }


    private void ResetWaveDistance()
    {
        isWaving = false;
        newWaveDistance = -0.1f;
        newWaveStrength = -0.15f;
        material.SetFloat(waveDistanceFromCenter, -0.1f);
        material.SetFloat(waveStrength, -0.15f);
    }

    private Vector2 GetWaveStartPosition()
    {
        Vector2 screenPos = camera.WorldToScreenPoint(player.transform.position);
        Vector2 screenPercentPosition = new Vector2(screenPos.x / Screen.width, screenPos.y / Screen.height);

        return screenPercentPosition;
    }
}
