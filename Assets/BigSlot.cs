using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BigSlot : MonoBehaviour
{
    public Image image;
    public GameObject nameText;
    public GameObject descriptionText;
    public GameObject changeAspectButton;
    public Image changeAspectImage;

    private CollectableData currentData;

    private void Awake()
    {
        SettingsManger.LanguageUpdated += UpdateItemTranslation;
    }

    private void OnEnable()
    {
        SetDefaultItem(PauseMenu.Instance.defaultCollectible);
    }

    public void UpdateItem(CollectableData collectableData)
    {
        currentData = collectableData;
        image.sprite = collectableData.redVersion;

        nameText.GetComponent<TextMeshProUGUI>().text = GetTranslation((int)SettingsManger.currentLanguage, collectableData.keyName);
        descriptionText.GetComponent<TextMeshProUGUI>().text = GetTranslation((int)SettingsManger.currentLanguage, collectableData.keyDescription);

        if (currentData.id == 0 || currentData.id == 3) // id of green & red seeds
        {
            changeAspectButton.SetActive(false);
            return;
        }
        
        changeAspectButton.SetActive(true);
        changeAspectImage.sprite = currentData.greenIcon;
    }

    public void ChangeItemAspect()
    {
        if (image.sprite == currentData.redVersion)
        {
            image.sprite = currentData.greenVersion;
            changeAspectImage.sprite = currentData.redIcon;
        }
        else if (image.sprite == currentData.greenVersion)
        {
            image.sprite = currentData.redVersion;
            changeAspectImage.sprite = currentData.greenIcon;
        }
    }

    public void SetDefaultItem(CollectableData collectableData)
    {
        UpdateItem(collectableData);
    }

    private string GetTranslation(int languageId, string key)
    {
        if (key == null || 
            key == "" ||
            StringDataReader.Instance == null ||
            StringDataReader.TranslateDictionary == null ||
            StringDataReader.TranslateDictionary.Count == 0)
        {
            return "";
        }

        return StringDataReader.TranslateDictionary.GetValueOrDefault(key)[languageId];
    }

    private void UpdateItemTranslation(int languageId)
    {
        nameText.GetComponent<TextMeshProUGUI>().text = GetTranslation((int)SettingsManger.currentLanguage, currentData.keyName);
        descriptionText.GetComponent<TextMeshProUGUI>().text = GetTranslation((int)SettingsManger.currentLanguage, currentData.keyDescription);
    }
}
