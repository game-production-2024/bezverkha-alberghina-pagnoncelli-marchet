using UnityEngine;
using UnityEngine.UI;

public class StaminaBar : MonoBehaviour
{
    public Slider staminaBar;
    public Canvas Canvas;
    public GameObject player;
    public Vector2 staminaBarOffsetLeft = new Vector2(-40f, 25f);
    public Vector2 staminaBarOffsetRight = new Vector2(40f, 25f);

    private PlayerStateController PlayerController;
    private Camera Camera;
    private RectTransform CanvasRect;
    private RectTransform StaminaBarRect;

    private void Awake()
    {
        Camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        PlayerController = player.GetComponent<PlayerStateController>();
        CanvasRect = Canvas.GetComponent<RectTransform>();
        StaminaBarRect = gameObject.GetComponent<RectTransform>();

        staminaBar = GetComponent<Slider>();
        staminaBar.maxValue = PlayerController.maxStamina;
        staminaBar.value = PlayerController.StaminaCounter;
    }

    private void Update()
    {
        CalculateScreenPosition();
    }

    private void CalculateScreenPosition()
    {
        Vector2 ViewportPosition = Camera.WorldToViewportPoint(player.transform.position);
        Vector2 WorldObject_ScreenPosition = new Vector2(
        ((ViewportPosition.x * CanvasRect.sizeDelta.x) - (CanvasRect.sizeDelta.x * 0.5f)),
        ((ViewportPosition.y * CanvasRect.sizeDelta.y) - (CanvasRect.sizeDelta.y * 0.5f)));

        if (PlayerController.isFacingRight)
        {
            StaminaBarRect.anchoredPosition = WorldObject_ScreenPosition + staminaBarOffsetLeft;
        }
        else
        {
            StaminaBarRect.anchoredPosition = WorldObject_ScreenPosition + staminaBarOffsetRight;
        }
    }

    public void SetStamina(float stamina)
    {
        staminaBar.value = stamina;
        var isBarVisible = stamina != staminaBar.maxValue;

        // Forced position recalculation to avoid visual bug
        if (isBarVisible == true)
        {
            CalculateScreenPosition();
        }

        gameObject.SetActive(isBarVisible); // Activate stamina bar only if it's not full
    }
}
