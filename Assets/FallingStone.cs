using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor.Rendering;
using UnityEngine;

public class FallingStone : MonoBehaviour
{
    public List<Sprite> sprites = new List<Sprite>();
    public float fallSpeed = 3f;

    public bool randomRotation = true;

    private void Awake()
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = sprites[Random.Range(0, sprites.Count)];

        if (randomRotation == true)
        {
            transform.eulerAngles = new Vector3(0, 0, Random.Range(0, 360));
        }
    }

    private void Update()
    {
        transform.Translate(Vector2.down * fallSpeed * Time.deltaTime, Space.World);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "StoneDestroyer")
        {
            Destroy(gameObject);
        }
    }
}
