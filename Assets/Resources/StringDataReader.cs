using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class StringDataReader : MonoBehaviour
{
    public static StringDataReader Instance { get; private set; }

    private TextAsset txtFile; // Reference of CSV file

    private char lineSeperater = '\n'; // It defines line seperate character
    private char fieldSeperator = '\t'; // It defines field seperate chracter

    private string[] records;
    private string[] fields;

    public static Dictionary<string, string[]> TranslateDictionary = new Dictionary<string, string[]>();

    private void Awake()
    {
        if (Instance != null && Instance != this) 
        { 
            Destroy(this); 
        } 
        else 
        { 
            Instance = this; 
        } 

        txtFile = Resources.Load<TextAsset>("Localization_Dictionary");
        TranslateDictionary.Clear();

        if (txtFile == null)
        {
            Debug.LogWarning("Localization file is not found!");
            return;
        }

        ReadData();
    }
    
    // Read data from TXT file
    private void ReadData()
    {
        var rawRecords = txtFile.text.Split(lineSeperater); // separate records in the file
        records = rawRecords.Skip(1).ToArray(); // skip the first record (title of each language)

        foreach (string record in records)
        {
            fields = record.Split(fieldSeperator);
            
            var key = fields[0]; // field[0] in each recod = unique phrase key
            var value = new string[fields.Length - 1]; // crete an array with a dimention = number of languages
            value = fields.Skip(1).ToArray(); // skip the first (key) element
            
            TranslateDictionary.Add(key, value); // add keys and values to the dictionary
        }
    }
}
