using UnityEngine;
using UnityEngine.UI;

public class CollectionSlot : MonoBehaviour
{
    public CollectableData collectableData;
    public GameObject icon;
    private Image iconImage;
    private Button button;

    private bool _isInCollection = false;

    public bool IsInCollection
    {
        get => _isInCollection;
        set
        {
            icon.SetActive(value);

            if (button == null)
            {
                button = GetComponent<Button>();
            }
            button.interactable = value;
            _isInCollection = value;
        }
    }

    private void Awake ()
    {
        button = GetComponent<Button>();
        iconImage = icon.GetComponent<Image>();
        iconImage.sprite = collectableData.redIcon;
    }
}
