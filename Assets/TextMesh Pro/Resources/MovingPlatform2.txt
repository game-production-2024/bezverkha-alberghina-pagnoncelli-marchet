using DG.Tweening;
using System.Collections;
using UnityEngine;

public class PlatformMovement : MonoBehaviour
{
    public float moveTime = 3f; // Velocità di movimento della piattaforma
    public float moveDistance = 5f; // Distanza massima di spostamento dalla posizione iniziale
    public float waitTime = 2f; // Tempo di attesa alle estremità

    public bool startRight = true; // Se true, la piattaforma inizierà a muoversi a destra

    private float minX;
    private float maxX;

    private bool startLoop = false;

    private void Awake()
    {
        minX = transform.position.x - moveDistance;
        maxX = transform.position.x + moveDistance;
    }

    // Metodo chiamato quando il gioco inizia
    private void Start()
    {
        transform.DOMoveX(maxX, moveTime).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);
    }
}