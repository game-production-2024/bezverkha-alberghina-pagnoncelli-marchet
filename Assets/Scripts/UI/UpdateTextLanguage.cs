using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UpdateTextLanguage : MonoBehaviour
{
    [SerializeField] private string textKey;
    private TextMeshProUGUI textMeshProUGUI;

    private void OnEnable() 
    {
        textMeshProUGUI = GetComponent<TextMeshProUGUI>();
        if (textMeshProUGUI == null)
        {
            return;
        }

        UpdateText((int)SettingsManger.currentLanguage);
        SettingsManger.LanguageUpdated += UpdateText;
    }

    private void UpdateText(int languageId)
    {
        if (textKey == null || 
            textKey == "" || 
            textMeshProUGUI == null || 
            StringDataReader.Instance == null ||
            StringDataReader.TranslateDictionary == null ||
            StringDataReader.TranslateDictionary.Count == 0)
        {
            return;
        }

        var newString = StringDataReader.TranslateDictionary.GetValueOrDefault(textKey)[languageId];
        textMeshProUGUI.text = newString;
    }
}
