using UnityEngine;
using UnityEngine.Events;

public class SettingsManger : MonoBehaviour
{
    public static SettingsManger Instance { get; private set; }

    public enum Language {Eng, Ita, Rus, Ukr}; 

    public static UnityAction<int> LanguageUpdated;
    
    public Language defaultLanguage = Language.Eng;
    public static Language currentLanguage;


    private void Awake() 
    {     
        if (Instance != null && Instance != this) 
        { 
            Destroy(this); 
        } 
        else 
        { 
            Instance = this; 
        }

        SetDefaultLanguage();
    }

    private void SetDefaultLanguage()
    {
        currentLanguage = defaultLanguage;
    }

    public void SetEngLanguage()
    {
        currentLanguage = Language.Eng;
        LanguageUpdated?.Invoke(0);
    }

    public void SetItaLanguage()
    {
        currentLanguage = Language.Ita;
        LanguageUpdated?.Invoke(1);
    }

    public void SetRusLanguage()
    {
        currentLanguage = Language.Rus;
        LanguageUpdated?.Invoke(2);
    }

    public void SetUkrLanguage()
    {
        currentLanguage = Language.Ukr;
        LanguageUpdated?.Invoke(3);
    }
}
