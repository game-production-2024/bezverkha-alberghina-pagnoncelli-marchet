using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NewItemMessage : MonoBehaviour
{
    public GameObject imageObject;
    public GameObject textObject;

    public string defaultMessage = "New item: ";
    public string defaultMessageKey = "";

    private string itemNameKey;

    private void Awake()
    {
        SettingsManger.LanguageUpdated += UpdateDefaultMessage;
    }

    public void Initialize(Sprite image, string nameKey)
    {
        itemNameKey = nameKey;
        
        imageObject.GetComponent<Image>().sprite = image;
        UpdateDefaultMessage((int)SettingsManger.currentLanguage);
    }
    
    private void UpdateDefaultMessage(int languageId)
    {
        if (defaultMessageKey == null || 
            defaultMessageKey == "" ||
            itemNameKey == null ||
            itemNameKey == ""||
            StringDataReader.Instance == null ||
            StringDataReader.TranslateDictionary == null ||
            StringDataReader.TranslateDictionary.Count == 0)
        {
            return;
        }

        var newMessage = StringDataReader.TranslateDictionary.GetValueOrDefault(defaultMessageKey)[languageId];
        var newItemName = StringDataReader.TranslateDictionary.GetValueOrDefault(itemNameKey)[languageId];

        textObject.GetComponent<TextMeshProUGUI>().text = newMessage + " " + newItemName;
    }

    private void OnDestroy() 
    {
        SettingsManger.LanguageUpdated -= UpdateDefaultMessage;
    }
}
