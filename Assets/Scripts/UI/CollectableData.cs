using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

[CreateAssetMenu]
public class CollectableData : ScriptableObject
{
    public int id = 0;
    public string keyName;
    public string keyDescription;
    public Sprite redVersion;
    public Sprite greenVersion;
    public Sprite redIcon;
    public Sprite greenIcon;
}
