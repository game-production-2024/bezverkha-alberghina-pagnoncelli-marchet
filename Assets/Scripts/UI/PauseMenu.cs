using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static PauseMenu Instance { get; private set; }

    public string scenaPrincipale = "Main Menu";
    public GameObject Pause;
    public GameObject Options;
    public GameObject Collectibles;
    public GameObject Controls;
    public KeyCode toggleKey = KeyCode.Escape;
    public bool aperto;

    public List<CollectionSlot> collectibleSlots = new List<CollectionSlot>();
    public CollectableData defaultCollectible;
    public CollectionSlot greenSeed;
    public CollectionSlot redSeed;

    private void Awake()
    {
        // Singleton pattern implementation
        if (Instance != null && Instance != this) 
        { 
            Destroy(this); 
        } 
        else 
        { 
            Instance = this;
            DontDestroyOnLoad(gameObject); // Prevents the object from being destroyed when changing scenes
        } 

        Scene currentScene = SceneManager.GetActiveScene();
        if (currentScene.name == "Alpha Stage 0")
        {
            GetGreenSeed();
            return;
        }

        GetRedSeed();
    }

    void Start()
    {
        GetGreenSeed();

        Pause.SetActive(false);
        Options.SetActive(false);
        Collectibles.SetActive(false);
        Controls.SetActive(false);
        aperto = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance?.playerOnDialogue == true)
        {
            return;
        }

        if (GameManager.Instance?.playerDead == true)
        { 
            return;
        }

        if (Input.GetKeyUp(toggleKey) && SceneManager.GetActiveScene().name != "Main Menu")
        {
            GameManager.Instance.PlayerOnDialogue();
            AudioManager.Instance.PlaySFX(AudioManager.Instance.menuopen);
            Pause.SetActive(!Pause.activeSelf);
            aperto = Pause.activeSelf;

            if (Options.activeSelf || Collectibles.activeSelf)
            {
                Return();
                return;
            }

            if (Pause.activeSelf)
            {
                Time.timeScale = 0f;
                return;
            }

            Time.timeScale = 1f;
        }
    }

    public void GetGreenSeed()
    {
        defaultCollectible = greenSeed.collectableData;

        greenSeed.IsInCollection = true;
        greenSeed.gameObject.SetActive(true);
        redSeed.IsInCollection = false;
        redSeed.gameObject.SetActive(false);
    }

    public void GetRedSeed()
    {
        defaultCollectible = redSeed.collectableData;

        greenSeed.IsInCollection = false;
        greenSeed.gameObject.SetActive(false);
        redSeed.IsInCollection = true;
        redSeed.gameObject.SetActive(true);
    }

    public void OptionsMenu()
    {
        AudioManager.Instance.PlaySFX(AudioManager.Instance.menuclose);
        Pause.SetActive(false);
        Collectibles.SetActive(false);
        Options.SetActive(true);
        Controls.SetActive(false);
    }

    public void CollectiblesMenu()
    {
        AudioManager.Instance.PlaySFX(AudioManager.Instance.menuclose);
        Pause.SetActive(false);
        Options.SetActive(false);
        Collectibles.SetActive(true);
        Controls.SetActive(false);
    }
    public void ControlsMenu()
    {
        AudioManager.Instance.PlaySFX(AudioManager.Instance.menuclose);
        Pause.SetActive(false);
        Options.SetActive(false);
        Collectibles.SetActive(false);
        Controls.SetActive(true);
    }

    public void Quit()
    {
        AudioManager.Instance.PlaySFX(AudioManager.Instance.menuclose);
        Application.Quit();
        Debug.Log("Gioco Chiuso");
    }

    public void Return()
    {
        AudioManager.Instance.PlaySFX(AudioManager.Instance.menuclose);
        Options.SetActive(false);
        Collectibles.SetActive(false);
        Pause.SetActive(true);
        Controls.SetActive(false);
    }

    public void Resume()
    {
        AudioManager.Instance.PlaySFX(AudioManager.Instance.menuclose);
        GameManager.Instance.PlayerFinishedDialogue();
        CloseAllMenues();
        Time.timeScale = 1f;
        aperto = false;
    }

    public void ToMenu()
    {
        AudioManager.Instance.PlaySFX(AudioManager.Instance.menuclose);
        CloseAllMenues();
        SceneManager.LoadScene(scenaPrincipale);
        Time.timeScale = 1f;
    }

    private void CloseAllMenues()
    {
        AudioManager.Instance.PlaySFX(AudioManager.Instance.menuclose);
        Options.SetActive(false);
        Collectibles.SetActive(false);
        Pause.SetActive(false);
        Controls.SetActive(false);
    }

    public void ActivateCollectibleButton(CollectableData collectableData)
    {
        var slotToActivate = collectibleSlots.Find( x => x.collectableData==collectableData);
        if (slotToActivate != null)
        {
            slotToActivate.IsInCollection = true;
        }
    }
}