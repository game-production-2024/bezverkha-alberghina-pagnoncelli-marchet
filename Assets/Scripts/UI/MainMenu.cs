using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public string scenaPrincipale = "Cutscene";
    public GameObject Optionmenu;
    public GameObject Buttons;
    public GameObject Credits;
    public GameObject Selection;

    private AudioClip clip;

    private void Start()
    {
        AudioManager.Instance.PlayMusic(AudioManager.Instance.intro);
        Optionmenu.SetActive(false);
        Credits.SetActive(false);
        Selection.SetActive(false);
    }

    public void PlayGame()
    {
        AudioManager.Instance.StopMusic(clip);
        SceneManager.LoadScene(scenaPrincipale);
        Time.timeScale = 1f;
    }

    public void OpenOptions()
    {
        Buttons.SetActive(false);
        Optionmenu.SetActive(true);
    }

    public void OpenCredits()
    {
        Buttons.SetActive(false);
        Credits .SetActive(true);
    }



    public void QuitGame()
    {
        Application.Quit();
    }

    public void Return()
    {
        Buttons.SetActive(true);
        Optionmenu.SetActive(false);
        Credits.SetActive(false);
    }

    public void TheMouth()
    {
        PauseMenu.Instance.GetGreenSeed();
        SceneManager.LoadScene("Alpha Stage 0");
        AudioManager.Instance.StopMusic(clip);
        AudioManager.Instance.PlayMusic(AudioManager.Instance.background0);
    }

    public void TheStomach()
    {
        PauseMenu.Instance.GetRedSeed();
        SceneManager.LoadScene("Alpha Stage 1");
        AudioManager.Instance.StopMusic(clip);
        AudioManager.Instance.PlayMusic(AudioManager.Instance.background1);
    }

    public void TheLungs()
    {
        PauseMenu.Instance.GetRedSeed();
        SceneManager.LoadScene("Beta Stage 2");
        AudioManager.Instance.StopMusic(clip);
        AudioManager.Instance.PlayMusic(AudioManager.Instance.background2);
    }

    public void TheThroat()
    {
        PauseMenu.Instance.GetRedSeed();
        SceneManager.LoadScene("Beta Stage 3");
        AudioManager.Instance.StopMusic(clip);
        AudioManager.Instance.PlayMusic(AudioManager.Instance.background3);
    }
}
