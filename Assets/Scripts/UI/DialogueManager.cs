using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DialogueManager : MonoBehaviour
{
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI dialogueText;
    public GameObject Textbox;
    public bool DialogoAperto;
    public GameObject Button;
    public KeyCode toggleKey = KeyCode.Space;

    public Queue<string> sentences;

    private Dialogue currentDialogue;
    private GameObject currentDialogueObject; // Riferimento all'oggetto che ha innescato il dialogo

    private void Awake()
    {
        UpdateNameText((int)SettingsManger.currentLanguage);
        UpdateDialogueSequence((int)SettingsManger.currentLanguage);
        
        SettingsManger.LanguageUpdated += UpdateNameText;
        SettingsManger.LanguageUpdated += UpdateDialogueSequence;
    }

    void Start()
    {
        sentences = new Queue<string>();
        DialogoAperto = false;
    }

    private void Update()
    {

        if (Input.GetKeyUp(toggleKey) && DialogoAperto == true)
        {
            DisplayNextSentence();
        }
    }

    public void StartDialogue(Dialogue dialogue, GameObject dialogueObject)
    {
        DialogoAperto = true;
        GameManager.Instance.PlayerOnDialogue();
        GameManager.Instance.PlayerCannotOpenMenu();

        currentDialogue = dialogue;
        currentDialogueObject = dialogueObject; // Memorizziamo l'oggetto che ha innescato il dialogo

        UpdateNameText((int)SettingsManger.currentLanguage);
        UpdateDialogueSequence((int)SettingsManger.currentLanguage);

        nameText.text = dialogue.name;

        sentences.Clear();

        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();

    }

    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }

        string sentence = sentences.Dequeue();
        dialogueText.text = sentence;
    }

    void EndDialogue()
    {
        Debug.Log("End of conversation");

        GameManager.Instance.PlayerFinishedDialogue();
        GameManager.Instance.PlayerCanOpenMenu();

        // Se l'oggetto del dialogo � un Mammuth, notifica il GameManager
        if (currentDialogueObject != null && currentDialogueObject.CompareTag("Mammuth"))
        {
            GameManager.Instance.UnlockDash();
        }

        if (currentDialogueObject != null && currentDialogueObject.CompareTag("Owl"))
        {
            GameManager.Instance.LockDash();
            GameManager.Instance.UnlockIPD();
        }

        Time.timeScale = 1f;
        Textbox.gameObject.SetActive(false);
        Button.gameObject.SetActive(false);
        currentDialogue = null;
        currentDialogueObject = null; // Ripristina il riferimento all'oggetto
    }

    private void UpdateNameText(int languageId)
    {
        if (currentDialogue == null ||
            currentDialogue.nameKey == null ||
            currentDialogue.nameKey == "" ||
            nameText == null ||
            StringDataReader.Instance == null ||
            StringDataReader.TranslateDictionary == null ||
            StringDataReader.TranslateDictionary.Count == 0)
        {
            return;
        }

        var newString = StringDataReader.TranslateDictionary.GetValueOrDefault(currentDialogue.nameKey)[languageId];
        currentDialogue.name = newString;
    }

    private void UpdateDialogueSequence(int languageId)
    {
        if (currentDialogue == null ||
            currentDialogue.nameKey == null ||
            currentDialogue.nameKey == "" ||
            nameText == null ||
            StringDataReader.Instance == null ||
            StringDataReader.TranslateDictionary == null ||
            StringDataReader.TranslateDictionary.Count == 0)
        {
            return;
        }

        for (int i = 0; i < currentDialogue.sentences.Length; i++)
        {
            var newSentence = StringDataReader.TranslateDictionary.GetValueOrDefault(currentDialogue.sentencesKeys[i])[languageId];
            currentDialogue.sentences[i] = newSentence;
        }
    }
}
