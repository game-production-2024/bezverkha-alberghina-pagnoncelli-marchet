﻿using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{

    public Dialogue dialogue;
    public GameObject Textbox;
    public KeyCode toggleKey = KeyCode.E;
    public bool Triggered;
    public GameObject Button;
    public bool InDialogue;

    public Sprite defaultSprite;
    public Sprite highlightedSprite;

    public Canvas Canvas;
    public Vector2 TextBoxOffset = new Vector2(0f, 10f);
    public Vector2 ButtonOffset = new Vector2(0f, 10f);
    private Camera Camera;
    private SpriteRenderer spriteRenderer;
    private GameObject player;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        Camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        spriteRenderer.sprite = defaultSprite;
        Textbox.gameObject.SetActive(false);
        Button.gameObject.SetActive(false);
        Triggered = false;
        InDialogue = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(toggleKey) && (Triggered == true) && (InDialogue == false))
        {
            Button.gameObject.SetActive(false);
            Textbox.gameObject.SetActive(true);
            InDialogue = true;
            TriggerDialogue();
        }
    }
    public void TriggerDialogue()
    {
        spriteRenderer.sprite = defaultSprite;

        DialogueManager dialogueManager = FindObjectOfType<DialogueManager>();
        if (dialogueManager != null)
        {
            // Passa anche il GameObject corrente (this.gameObject) come secondo parametro
            dialogueManager.StartDialogue(dialogue, this.gameObject);
            Time.timeScale = 0f;
        }
    }

    public void Enter() 
    {
        CalculateScreenPosition();

        Button.gameObject.SetActive(true);
        Triggered = true;
        spriteRenderer.sprite = highlightedSprite;
    }

    public void Exit() 
    { 
        spriteRenderer.sprite = defaultSprite;
        Button.gameObject.SetActive(false);
        Textbox.gameObject.SetActive(false);
        Triggered = false;
        InDialogue = false;
    }

    private void CalculateScreenPosition()
    {
        RectTransform CanvasRect = Canvas.GetComponent<RectTransform>();

        //then you calculate the position of the UI element
        //0,0 for the canvas is at the center of the screen, whereas WorldToViewPortPoint treats the lower left corner as 0,0. Because of this, you need to subtract the height / width of the canvas * 0.5 to get the correct position.

        Vector2 ViewportPosition = Camera.WorldToViewportPoint(gameObject.transform.position);
        Vector2 WorldObject_ScreenPosition = new Vector2(
        ((ViewportPosition.x * CanvasRect.sizeDelta.x) - (CanvasRect.sizeDelta.x * 0.5f)),
        ((ViewportPosition.y * CanvasRect.sizeDelta.y) - (CanvasRect.sizeDelta.y * 0.5f)));

        //now you can set the position of the ui element
        Textbox.GetComponent<RectTransform>().anchoredPosition = WorldObject_ScreenPosition  + TextBoxOffset;
        Button.GetComponent<RectTransform>().anchoredPosition = WorldObject_ScreenPosition  + ButtonOffset;
    }
}
