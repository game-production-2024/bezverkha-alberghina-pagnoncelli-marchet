using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hide : MonoBehaviour
{
    public GameObject Tutorial;
    void Start()
    {
        Tutorial.gameObject.SetActive(true);
    }

    public void HideObject()
    {
        Tutorial.gameObject.SetActive(false);
    }
    void Update()
    {
        
    }
}
