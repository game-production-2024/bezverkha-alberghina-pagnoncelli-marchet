using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class CutscenePlayer : MonoBehaviour
{

    public PlayableDirector playableDirector;
   // public GameObject CutsceneCamera;
   // public GameObject GameCamera;




    void Start()
    {
        if (playableDirector == null)
        {
            playableDirector = GetComponent<PlayableDirector>();
        }

        if (playableDirector != null)
        {
                playableDirector.Pause();
        }
    }


    void Update()
    {

    }

    public void PlayCutscene ()
    {
      //  CutsceneCamera.SetActive(true);
      //  GameCamera.SetActive(false);
        if (playableDirector != null)
        {
            AudioManager.Instance.StopLoopSFXGeneric();

            playableDirector.Play();
        }
    }
}
