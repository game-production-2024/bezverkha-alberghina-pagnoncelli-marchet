using UnityEngine;

public class SelfDestroyTimer : MonoBehaviour
{
    public float timeToDestroy = 3f;

    void Update()
    {
        timeToDestroy -= Time.deltaTime;

        if (timeToDestroy <= 0)
        {
            Destroy(gameObject);
        }
    }
}
