using UnityEngine;
using UnityEngine.UIElements;

public class StalactitActivation : MonoBehaviour
{
    public GameObject currentStalactit;
    public GameObject stalactitPref;
    public float timeToRespawn = 3f;

    private Vector2 stalactitSpawnPosition;
    private float timer = 0;
    private bool startTimerToRespawn = false;

    private void Awake()
    {
        timer = timeToRespawn;
        stalactitSpawnPosition = currentStalactit.transform.localPosition;
    }

    private void Update()
    {
        if (startTimerToRespawn == false)
        {
            return;
        }

        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }
        else
        {
            RespawnStalactit();
            timer = timeToRespawn;
            startTimerToRespawn = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.tag == "Player")
        {
            ActivateStalactit();
        }
    }

    private void ActivateStalactit()
    {
        if (currentStalactit == null)
        {
            return;
        }

        currentStalactit.GetComponent<Rigidbody2D>().gravityScale = 1;
        currentStalactit.GetComponent<SelfDestroyTimer>().enabled = true;
        startTimerToRespawn = true;
    }

    private void RespawnStalactit()
    {
        currentStalactit = Instantiate(stalactitPref, transform.parent);
        currentStalactit.transform.SetParent(transform.parent);
        currentStalactit.transform.localPosition = stalactitSpawnPosition;
    }
}
