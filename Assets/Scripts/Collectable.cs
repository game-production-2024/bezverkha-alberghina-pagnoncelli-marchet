using UnityEngine;
using DG.Tweening;

public class Collectable : MonoBehaviour
{
    [SerializeField] private CollectableData collectableData; 
    [SerializeField] private GameObject collectParticlePref;
    [SerializeField] private GameObject newItemFoundPref;

    [SerializeField] private float floatSpeed = 1.5f;
    [SerializeField] private float floatDistance = 1.5f;
    [SerializeField] private Ease floatMoveMode = Ease.Linear;


    private void Awake()
    {
        GetComponent<SpriteRenderer>().sprite = collectableData.redIcon;
    }

    private void Start()
    {
                transform.DOMoveY(transform.position.y + floatDistance, floatSpeed)
                .SetEase(floatMoveMode).SetLoops(-1, LoopType.Yoyo);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            AudioManager.Instance.PlaySFX(AudioManager.Instance.getcollectible);
            SpawnParticle();
            SpawnNewItemFoundMessage();
            Destroy(gameObject);
            PauseMenu.Instance.ActivateCollectibleButton(collectableData);
        }
    }

    private void SpawnParticle()
    {
        if (collectParticlePref == null)
        {
            return;
        }

        Instantiate(collectParticlePref, transform.position, transform.rotation);
    }

    private void SpawnNewItemFoundMessage()
    {
        if (newItemFoundPref == null)
        {
            return;
        }

        var messageBox = Instantiate(newItemFoundPref);
        messageBox.GetComponent<NewItemMessage>().Initialize(collectableData.redIcon, collectableData.keyName);
    }
}
