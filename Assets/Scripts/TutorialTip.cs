using UnityEngine;

public class TutorialTip : MonoBehaviour
{
    public GameObject TutorialObj;

    public RectTransform Textbox;

    public Canvas Canvas;
    private Camera Camera;

    //public Vector2 TextBoxOffset = new Vector2(0f, 10f);


    private void Start()
    {
        Camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        Textbox.gameObject.SetActive(false);
    }

    private void CalculateScreenPosition()
    {
        RectTransform CanvasRect = Canvas.GetComponent<RectTransform>();

        if (Camera == null)
        {
            return;
        }

        Vector2 ViewportPosition = Camera.WorldToViewportPoint(TutorialObj.transform.position);
        Vector2 WorldObject_ScreenPosition = new Vector2(
        ((ViewportPosition.x * CanvasRect.sizeDelta.x) - (CanvasRect.sizeDelta.x * 0.5f)),
        ((ViewportPosition.y * CanvasRect.sizeDelta.y) - (CanvasRect.sizeDelta.y * 0.5f)));

        //Textbox.anchoredPosition = WorldObject_ScreenPosition  + TextBoxOffset;
        Textbox.anchoredPosition = WorldObject_ScreenPosition;
    }

    public void ShowTutorialBox(bool value)
    {
        CalculateScreenPosition();
        Textbox.gameObject.SetActive(value);
    }
}
