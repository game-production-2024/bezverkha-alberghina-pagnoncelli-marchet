using UnityEngine;
using UnityEngine.UI;

public class SliderSFX : MonoBehaviour
{
    [SerializeField] private Slider sfxSlider;

    private void Start()
    {
        // Carica il valore salvato per gli SFX (o imposta un valore di default di 1 se non esiste)
        float savedSFXVolume = PlayerPrefs.GetFloat("SFXVolume", 1f);

        // Imposta il valore dello slider e il volume delle sorgenti audio
        sfxSlider.value = savedSFXVolume;
        SetSFXVolume(savedSFXVolume);

        // Assegna l'evento OnValueChanged al metodo SetSFXVolume
        sfxSlider.onValueChanged.AddListener(SetSFXVolume);
    }

    // Metodo per aggiornare il volume degli SFX
    public void SetSFXVolume(float volume)
    {
        // Aggiorna il volume per ogni sorgente nel pool di SFX
        foreach (AudioSource source in AudioManager.Instance.sfxSources)
        {
            source.volume = volume;
        }

        // Aggiorna anche il volume della sorgente loop SFX
        if (AudioManager.Instance.loopSFXSource != null)
        {
            AudioManager.Instance.loopSFXSource.volume = volume;
        }

        // Salva il volume attuale con PlayerPrefs
        PlayerPrefs.SetFloat("SFXVolume", volume);
    }
}

