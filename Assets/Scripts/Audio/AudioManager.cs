using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour
{
    // Singleton instance
    public static AudioManager Instance { get; private set; }

    [Header("------- Audio Source -------")]
    [SerializeField] public AudioSource musicSource;
    [SerializeField] public AudioSource loopSFXSource; // Separate source for looping SFX

    [Header("------- Audio Clip -------")]
    public AudioClip intro;
    public AudioClip background0;
    public AudioClip background1;
    public AudioClip background2;
    public AudioClip background3;
    public AudioClip death;
    public AudioClip checkpoint;
    public AudioClip climb;
    public AudioClip climbidle;
    public AudioClip dash;
    public AudioClip landing;
    public AudioClip ipdon;
    public AudioClip ipdoff;
    public AudioClip run;
    public AudioClip cameraswitch;
    public AudioClip interactiveskelly;
    public AudioClip getcollectible;
    public AudioClip jump;
    public AudioClip platformcrash;
    public AudioClip respawn;
    public AudioClip menuopen;
    public AudioClip menuclose;
    public AudioClip click;
    public AudioClip camerashake;

    private bool isPlayingLoopSFX = false;

    // Pool di AudioSource per SFX
    public List<AudioSource> sfxSources;
    [SerializeField] private int poolSize = 10; // Dimensione del pool

    private void Awake()
    {
        // Singleton pattern implementation
        if (Instance != null && Instance != this) 
        { 
            Destroy(this); 
        } 
        else 
        { 
            Instance = this;
        } 

        // Inizializza il pool di AudioSource per gli SFX
        sfxSources = new List<AudioSource>();
        for (int i = 0; i < poolSize; i++)
        {
            AudioSource newSource = gameObject.AddComponent<AudioSource>();
            sfxSources.Add(newSource);
        }

        LoadSavedVolumes();
    }

    private void Start()
    {
        var currentScene = SceneManager.GetActiveScene().name;

        switch (currentScene)
        {
        case "Alpha Stage 0":
                musicSource.clip = background0;
            break;

        case "Alpha Stage 1":
                musicSource.clip = background1;
            break;

        case "Beta Stage 2":
                musicSource.clip = background2;
            break;

        case "Beta Stage 3":
                musicSource.clip = background3;
            break;

            default:
                musicSource.clip = intro;
            break;

    }

        musicSource.Play();
    }

    public void LoadSavedVolumes()
    {
        // Carica il volume salvato per la musica (default 1.0)
        float savedMusicVolume = PlayerPrefs.GetFloat("MusicVolume", 1f);
        musicSource.volume = savedMusicVolume;

        // Carica il volume salvato per gli SFX (default 1.0)
        float savedSFXVolume = PlayerPrefs.GetFloat("SFXVolume", 1f);

        // Applica il volume a tutte le sorgenti audio SFX nel pool
        foreach (AudioSource source in sfxSources)
        {
            source.volume = savedSFXVolume;
        }

        // Applica anche il volume alla sorgente loop SFX
        if (loopSFXSource != null)
        {
            loopSFXSource.volume = savedSFXVolume;
        }
    }

    public void PlayMusic(AudioClip clip)
    {
        if (musicSource.clip != clip) // Cambia traccia solo se � diversa da quella attuale
        {
            musicSource.Stop();       // Ferma la traccia corrente
            musicSource.clip = clip;  // Imposta il nuovo clip
            musicSource.Play();       // Riproduce il nuovo clip
        }
    }

    public void StopMusic(AudioClip clip)
    {
        if (musicSource.isPlaying)
        {
            musicSource.Stop();
        }
    }

    // Metodo per riprodurre un effetto sonoro SFX
    public void PlaySFX(AudioClip clip)
    {
        AudioSource availableSource = GetAvailableSFXSource();
        if (availableSource != null)
        {
            availableSource.PlayOneShot(clip);
        }
    }

    // Metodo per ottenere un AudioSource libero dal pool
    private AudioSource GetAvailableSFXSource()
    {
        foreach (AudioSource source in sfxSources)
        {
            if (source != null && source.isPlaying)
            {
                return source;
            }
        }

        // Se nessun AudioSource � disponibile, ne creiamo uno nuovo e lo aggiungiamo al pool
        AudioSource newSource = gameObject.AddComponent<AudioSource>();
        sfxSources.Add(newSource);
        return newSource;
    }

    // Riproduce un suono SFX in loop
    public void PlayLoopSFX(AudioClip clip)
    {
        if (!isPlayingLoopSFX)
        {
            loopSFXSource.clip = clip;
            loopSFXSource.loop = true;
            loopSFXSource.Play();
            isPlayingLoopSFX = true;
        }
    }

    public void StopLoopSFXGeneric()
    {
        if (isPlayingLoopSFX)
        {
            loopSFXSource.Stop();
            isPlayingLoopSFX = false;
        }
    }

    // Stop il suono SFX in loop
    public void StopLoopSFX(AudioClip clip)
    {
        if (isPlayingLoopSFX)
        {
            loopSFXSource.Stop();
            isPlayingLoopSFX = false;
        }
    }
}
