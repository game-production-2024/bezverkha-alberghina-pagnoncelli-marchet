using UnityEngine;
using UnityEngine.UI;

public class SliderMusic : MonoBehaviour
{
    [SerializeField] private Slider musicSlider;

    private void Start()
    {
        // Carica il valore salvato per la musica (o imposta un valore di default di 1 se non esiste)
        float savedMusicVolume = PlayerPrefs.GetFloat("MusicVolume", 1f);

        // Imposta il valore dello slider e il volume dell'AudioSource
        musicSlider.value = savedMusicVolume;
        AudioManager.Instance.musicSource.volume = savedMusicVolume;

        // Assegna l'evento OnValueChanged al metodo SetMusicVolume
        musicSlider.onValueChanged.AddListener(SetMusicVolume);
    }

    // Metodo per aggiornare il volume della musica
    public void SetMusicVolume(float volume)
    {
        // Aggiorna il volume della sorgente audio della musica nell'AudioManager
        AudioManager.Instance.musicSource.volume = volume;

        // Salva il volume attuale con PlayerPrefs
        PlayerPrefs.SetFloat("MusicVolume", volume);
    }
}
