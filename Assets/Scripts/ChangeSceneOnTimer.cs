using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeSceneOnTimer : MonoBehaviour
{
    // Start is called before the first frame update
    public float ChangeTime;
    public string SceneName;
    public AudioClip backgroundClip;

    // Update is called once per frame
    void Update()
    {
        ChangeTime -= Time.deltaTime;
        if (ChangeTime <= 0)
        {
            Change();
        }

    }

    public void Change()
    {
        SceneManager.LoadScene(SceneName);
        if (AudioManager.Instance != null && backgroundClip != null)
        {
            AudioManager.Instance.PlayMusic(backgroundClip);
        }
    }
}
