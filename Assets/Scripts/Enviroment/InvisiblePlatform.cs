using UnityEngine;

public class InvisiblePlatform : MonoBehaviour
{
    private PlayerStateController player;
    private SpriteRenderer spriteRenderer;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStateController>();

        spriteRenderer.enabled = false;
    }

    private void Update()
    {
        if (player.ipdActive == false)
        {
            spriteRenderer.enabled = false;
        }
    }

   private void OnTriggerEnter2D(Collider2D other) 
   {
        if (other.gameObject.tag == "IPD")
        {
            spriteRenderer.enabled = true;
        } 
   }

      private void OnTriggerExit2D(Collider2D other) 
   {
        if (other.gameObject.tag == "IPD")
        {
            spriteRenderer.enabled = false;
        } 
   }
}
