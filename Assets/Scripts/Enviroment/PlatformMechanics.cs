using System.Collections;
using UnityEngine;

public class PlatformMechanics : MonoBehaviour
{
    public float fallSpeed = 5f; // Variabile per memorizzare la velocit� di caduta della piattaforma
    public float resetTime = 2f; // Variabile per il tempo di ripristino

    public GameObject platformHitbox;
    public GameObject deadlyPlatformHitbox;
    public GameObject climbingHitbox;

    private Vector2 initialPosition; // Variabile per memorizzare la posizione iniziale della piattaforma
    private Renderer platformRenderer;
    private Renderer hitboxRenderer;
    private Collider2D platformCollider;
    private Collider2D hitboxCollider; // Collider for the hitbox
    private Rigidbody2D rb;
    public AudioManager audioManager;

    // Metodo chiamato quando il gioco inizia
    private void Start()
    {
        // Memorizza la posizione iniziale della piattaforma
        initialPosition = transform.position;
        deadlyPlatformHitbox.SetActive(false);

        // Ottieni i componenti renderer e collider
        platformRenderer = GetComponent<Renderer>();
        platformCollider = GetComponent<Collider2D>();
        hitboxRenderer = GetComponentInChildren<Renderer>();
        hitboxCollider = GetComponentInChildren<Collider2D>(); // Assuming the hitbox is a child object
        rb = GetComponent<Rigidbody2D>();
    }

    private void Awake()
    {
        audioManager = GameObject.FindGameObjectWithTag("Audio").GetComponent<AudioManager>();
    }

    public void PlatformActivated()
    {
        rb.constraints &= ~RigidbodyConstraints2D.FreezePositionY;
        deadlyPlatformHitbox.SetActive(true);
    }

    public void FallingPlatformTouching()
    {
        StartCoroutine(DestroyAndReset());
    }

    // Metodo chiamato quando un collider entra in collisione con un altro collider
    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Verifica se il collider con cui si � scontrata � il terreno
        if (collision.gameObject.CompareTag("Floor"))
        {
            StartCoroutine(DestroyAndReset());
        }
    }

    // Metodo chiamato quando un collider trigger entra in collisione con un altro collider
    private void OnTriggerEnter2D(Collider2D other)
    {
        // Verifica se il collider trigger con cui si � scontrata � il terreno
        if (other.gameObject.CompareTag("Floor"))
        {
            StartCoroutine(DestroyAndReset());
        }
    }

    // Metodo chiamato ad ogni frame
    private void Update()
    {

    }

    // Coroutine per distruggere e ripristinare la piattaforma
    public IEnumerator DestroyAndReset()
    {
        audioManager.PlaySFX(audioManager.platformcrash);

        // Disattiva il renderer e il collider della piattaforma
        platformRenderer.enabled = false;
        platformCollider.enabled = false;

        platformHitbox.SetActive(false);
        deadlyPlatformHitbox.SetActive(false);
        climbingHitbox.SetActive(false);
        //hitboxRenderer.enabled = false;
        //hitboxCollider.enabled = false; // Disattiva anche il collider trigger della hitbox

        // Aspetta per un determinato periodo di tempo
        yield return new WaitForSeconds(resetTime);

        // Ripristina la posizione iniziale della piattaforma
        transform.position = initialPosition;

        // Riattiva il renderer e i colliders della piattaforma
        platformRenderer.enabled = true;
        platformCollider.enabled = true;

        platformHitbox.SetActive(true);
        climbingHitbox.SetActive(true);
        //hitboxRenderer.gameObject.SetActive(false);
        //hitboxCollider.enabled = true; // Riattiva anche il collider trigger della hitbox
        rb.constraints |= RigidbodyConstraints2D.FreezePositionY;
    }
}
