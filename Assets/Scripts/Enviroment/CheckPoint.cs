using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    public GameObject CheckpointMessage;
    public GameObject particles;
    public Transform playerSpawnPoint;
    private Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        
        if (particles == null)
        {
            return;
        }

        particles.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (GameManager.Instance.lastCheckpointPosition.x == playerSpawnPoint.position.x &&
                GameManager.Instance.lastCheckpointPosition.y == playerSpawnPoint.position.y)
            {
                return;
            }

            SetPlayerSpawnPosition();

            var message = Instantiate(CheckpointMessage, transform);
            animator.SetBool("Active", true);
            AudioManager.Instance.PlaySFX(AudioManager.Instance.checkpoint);

            if (particles == null)
            {
                return;
            }

            particles.SetActive(true);
        }
    }

    private void SetPlayerSpawnPosition()
    {
        if (playerSpawnPoint != null)
        {
            GameManager.Instance.SetCheckpoint(playerSpawnPoint.position);
        }
        else
        {
            GameManager.Instance.SetCheckpoint(transform.position);
        }
    }
}
