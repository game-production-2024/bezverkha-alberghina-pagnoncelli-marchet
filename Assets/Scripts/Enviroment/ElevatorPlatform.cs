using UnityEngine;
using DG.Tweening;

public class ElevatorPlatform : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 3f; // Velocit� di movimento della piattaforma
    [SerializeField] private float waitTime = 1f; // Tempo di attesa alle estremit�
    [SerializeField] private bool smoothMove = true;
    [SerializeField] private Ease smoothMoveMode = Ease.OutQuad;

    [SerializeField] private Transform previewPlatform;

    private Vector2 initialPosition; // Posizione iniziale della piattaforma
    private Vector2 finalPosition; // Posizione finale della piattaforma
    private Vector2 targetPosition; // Target move position 

    private bool isMovingUp = false;
    private bool isWaiting = false;  // Indica se la piattaforma � in attesa
    private float returnTimer = 0;

    private float secureOffset = 0.07f; // serve per il sicuro funzionamento della position check


    // Metodo chiamato quando il gioco inizia
    private void Start()
    {
        initialPosition = transform.position;
        finalPosition = previewPlatform.position;
        targetPosition = finalPosition;

        returnTimer = waitTime;

        if (initialPosition.y < finalPosition.y)
        {
            isMovingUp = true;
        }
    }

    private void Update()
    {
        if (isMovingUp == false) //is going up
        {
            isWaiting = targetPosition.y > transform.position.y - secureOffset ? true : false;
        }
        else //is going up
        {
            isWaiting = targetPosition.y < transform.position.y + secureOffset ? true : false;
        }

        if (isWaiting == false)
        {
            switch (smoothMove)
            {
                case true:
                    transform.DOMove(targetPosition, moveSpeed).SetEase(smoothMoveMode);
                return;

                case false:
                    var step =  moveSpeed * Time.deltaTime;
                    transform.position = Vector3.MoveTowards(transform.position, targetPosition, step);
                return;
            }
        }

        if (isWaiting == true)
        {
            returnTimer -= Time.deltaTime;

            if (returnTimer <= 0)
            {
                isMovingUp = !isMovingUp;
                returnTimer = waitTime;
                targetPosition = targetPosition == finalPosition ? initialPosition : finalPosition;
            }
        }
    }
}