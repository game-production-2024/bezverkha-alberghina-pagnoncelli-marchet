using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{
    private AudioClip clip;

    public void ChangeScene(string sceneName)
    {
        if (AudioManager.Instance != null)
        {
            AudioManager.Instance.StopMusic(clip);
            AudioManager.Instance.StopLoopSFX(clip);
        }

        SceneManager.LoadScene(sceneName);
    }
}