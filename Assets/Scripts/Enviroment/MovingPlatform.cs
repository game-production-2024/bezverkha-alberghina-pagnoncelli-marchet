using UnityEngine;
using DG.Tweening;

public class PlatformMovement : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 2.5f; // Velocit� di movimento della piattaforma
    [SerializeField] private float waitTime = 2f; // Tempo di attesa alle estremit�
    [SerializeField] private bool smoothMove = true;
    [SerializeField] private Ease smoothMoveMode = Ease.OutQuad;

    [SerializeField] private Transform previewPlatform;

    private Vector2 initialPosition; // Posizione iniziale della piattaforma
    private Vector2 finalPosition; // Posizione finale della piattaforma
    private Vector2 targetPosition; // Target move position 

    private bool isMovingRight = false;
    private bool isWaiting = false;  // Indica se la piattaforma � in attesa
    private float returnTimer = 0;

    private float secureOffset = 0.07f; // serve per il sicuro funzionamento della position check


    // Metodo chiamato quando il gioco inizia
    private void Start()
    {
        initialPosition = transform.position;
        finalPosition = previewPlatform.position;
        targetPosition = finalPosition;

        returnTimer = waitTime;

        if (initialPosition.x < finalPosition.x)
        {
            isMovingRight = true;
        }
    }

    private void Update()
    {
        if (isMovingRight == false) //is going left
        {
            isWaiting = targetPosition.x > transform.position.x - secureOffset ? true : false;
        }
        else //is going right
        {
            isWaiting = targetPosition.x < transform.position.x + secureOffset ? true : false;
        }

        if (isWaiting == false)
        {
            switch (smoothMove)
            {
                case true:
                    transform.DOMove(targetPosition, moveSpeed).SetEase(smoothMoveMode);
                return;

                case false:
                    var step =  moveSpeed * Time.deltaTime;
                    transform.position = Vector3.MoveTowards(transform.position, targetPosition, step);
                return;
            }
        }

        if (isWaiting == true)
        {
            returnTimer -= Time.deltaTime;

            if (returnTimer <= 0)
            {
                isMovingRight = !isMovingRight;
                returnTimer = waitTime;
                targetPosition = targetPosition == finalPosition ? initialPosition : finalPosition;
            }
        }
    }
}

