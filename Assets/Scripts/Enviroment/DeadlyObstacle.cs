using UnityEngine;



public class DeadlyObstacle : MonoBehaviour
{
    public GameObject deathParticlePref;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            collision.gameObject.SetActive(false);
            AudioManager.Instance.PlaySFX(AudioManager.Instance.death);
            Debug.Log("Il giocatore � stato distrutto!");
            AudioManager.Instance.StopLoopSFXGeneric();
            GameManager.Instance.GameOver(true);

            if (deathParticlePref == null)
            {
                return;
            }

            Instantiate(deathParticlePref, collision.gameObject.transform.position, collision.gameObject.transform.rotation);
        }
    }
}
