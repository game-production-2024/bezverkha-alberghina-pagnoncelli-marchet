using UnityEngine;
using UnityEngine.SceneManagement; // Per gestire le scene

public class SceneTrigger : MonoBehaviour
{
    public string cutsceneEndStage0 = "End Tutorial"; // Nome della scena "Alpha Stage 1"
    public string cutsceneEndStage1 = "Intro Stage 2"; // Nome della scena "Alpha Stage 1"
    public string cutsceneEndStage2 = "Intro Stage 3"; // Nome della scena "Alpha Stage 1"
    public string cutsceneEndStage3 = "Ending"; // Nome della scena "Alpha Stage 1"
    public string cutsceneEndFinalStage = "Ending"; // Nome della scena "Alpha Stage 1"

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            // Ottiene la scena attualmente attiva
            Scene scenaCorrente = SceneManager.GetActiveScene();
            string nomeScenaCorrente = scenaCorrente.name;

            // Trova l'oggetto SceneChanger
            SceneChanger sceneChanger = FindObjectOfType<SceneChanger>();

            if (sceneChanger != null)
            {
                // Cambia scena in base alla scena corrente
                switch (nomeScenaCorrente)
                {
                    case "Alpha Stage 0":
                        sceneChanger.ChangeScene(cutsceneEndStage0); // Cambia in "Alpha Stage 1"
                        break;

                    case "Alpha Stage 1":
                        sceneChanger.ChangeScene(cutsceneEndStage1); // Cambia in "Alpha Stage 2"
                        break;

                    case "Beta Stage 2":
                        sceneChanger.ChangeScene(cutsceneEndStage2); // Cambia in "Alpha Stage 0" (loop)
                        break;

                    case "Beta Stage 3":
                        sceneChanger.ChangeScene(cutsceneEndStage3); // Cambia in "Alpha Stage 0" (loop)
                        break;

                    case "Beta Final Stage":
                        sceneChanger.ChangeScene(cutsceneEndFinalStage); // Cambia in "Alpha Stage 0" (loop)
                        break;

                    default:
                        Debug.LogError("Nome della scena corrente non riconosciuto!");
                        break;
                }
            }
            else
            {
                Debug.LogError("SceneChanger non trovato nella scena!");
            }

            PauseMenu.Instance.GetRedSeed();
        }
    }
}
