using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public static GameObject player;
    public float restartTime = 5f;

    public Vector2 lastCheckpointPosition;

    public bool playerOnDialogue = false;
    public bool playerDead = false;
    private bool canDash = true;
    private bool isDashing = false;
    private bool canUseIPD = true;
    private bool isUsingIPD = false;

    private bool timerStart = false;
    private float timeLeft = 0;
    private Scene currentScene;

    private void Start()
    {
        // Inizializza la posizione del checkpoint alla posizione iniziale del giocatore
        player = GameObject.FindGameObjectWithTag("Player");
        lastCheckpointPosition = player.transform.position;

        LockDash();
        LockIPD();
    }

    private void Awake()
    {
        // Singleton pattern implementation
        if (Instance != null && Instance != this) 
        { 
            Destroy(this); 
        } 
        else 
        { 
            Instance = this; 
        } 
    }

    private void Update()
    {
        if (timerStart == false)
        {
            return;
        }

        timeLeft -= Time.deltaTime;
        if (timeLeft <= 0)
        {
            timerStart = false;
            RespawnPlayer(player);
        }
    }

    public void SetCheckpoint(Vector2 checkpointPosition)
    {
        lastCheckpointPosition = checkpointPosition;
    }

    public void RespawnPlayer(GameObject playerObject)
    {
        playerObject.transform.position = lastCheckpointPosition;
        playerObject.SetActive(true);
        playerDead = false;

        PlayerStateController playerController = playerObject.GetComponent<PlayerStateController>();
        if (playerController != null)
        {
            playerController.SetState(new PlayerIdleState(playerController));
            playerController.canDash = canDash;
            playerController.IsDashing = isDashing;
            playerController.canUseIPD = canUseIPD;
            playerController.ipdActive = isUsingIPD;
        }

        AudioManager.Instance.PlaySFX(AudioManager.Instance.respawn);
    }

    public void GameOver(bool startDeathTimer)
    {
        playerDead = true;
        RestartTimer();
    }

    private void RestartTimer()
    {
        timeLeft = restartTime;
        timerStart = true;
    }

    public void PlayerOnDialogue()
    {
        PlayerStateController playerController = player.GetComponent<PlayerStateController>();
        if (playerController != null)
        {
            playerController.onDialogue = true;
        }
    }

    public void PlayerFinishedDialogue()
    {
        PlayerStateController playerController = player.GetComponent<PlayerStateController>();
        if (playerController != null)
        {
            playerController.onDialogue = false;
        }
    }

    public void PlayerCannotOpenMenu()
    {
        playerOnDialogue = true;
    }

    public void PlayerCanOpenMenu()
    {
        playerOnDialogue = false;
    }

    // Metodo per sbloccare la dash
    public void UnlockDash()
    {
        PlayerStateController playerController = player.GetComponent<PlayerStateController>();
        if (playerController != null)
        {
            playerController.dashUnlock = true; // Sblocca la dash
            Debug.Log("Dash sbloccata!");
        }
    }

    public void LockDash()
    {
        PlayerStateController playerController = player.GetComponent<PlayerStateController>();
        if (playerController != null)
        {
            playerController.dashUnlock = false; // Blocca la dash
            Debug.Log("Dash Bloccata!");
        }
    }

    public void UnlockIPD()
    {
        PlayerStateController playerController = player.GetComponent<PlayerStateController>();
        if (playerController != null)
        {
            playerController.ipdUnlock = true; // Sblocca l'ipd
            Debug.Log("IPD sbloccata!");
        }
    }

    public void LockIPD()
    {
        PlayerStateController playerController = player.GetComponent<PlayerStateController>();
        if (playerController != null)
        {
            playerController.ipdUnlock = false; // Blocca l'ipd
            Debug.Log("IPD Bloccata!");
        }
    }
}
