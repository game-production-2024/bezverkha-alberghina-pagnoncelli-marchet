using UnityEngine;

public class Inspectable : MonoBehaviour
{
    //this is your object that you want to have the UI element hovering over
    public GameObject InspectableObj;
    //this is the ui element
    public RectTransform Textbox;
    public RectTransform Button;

    public Canvas Canvas;
    private Camera Camera;
    
    public Vector2 TextBoxOffset = new Vector2(0f, 10f);
    public Vector2 ButtonOffset = new Vector2(0f, 10f);

    public KeyCode toggleKey = KeyCode.E; // Tasto modificabile nell'Inspector
    public bool Triggered;

    public Sprite defaultSprite;
    public Sprite highlightedSprite;
    private SpriteRenderer spriteRenderer;

    private void Start()
    {
        Camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        spriteRenderer = InspectableObj.GetComponent<SpriteRenderer>();

        spriteRenderer.sprite = defaultSprite;

        // Inizialmente la TextBox � nascosta
        Textbox.gameObject.SetActive(false);
        Button.gameObject.SetActive(false);
        Triggered = false;
    }

    private void Update()
    {
        //Controlla se il tasto specificato viene premuto
        if (Input.GetKeyDown(toggleKey) && (Triggered == false))
        {
            if (Button.gameObject.activeSelf == false)
            {
                return;
            }
            //Cambia la visibilit� dello sprite e della TextBox
            Button.gameObject.SetActive(false);
            Textbox.gameObject.SetActive(true);   
            spriteRenderer.sprite = defaultSprite; 
        }
    }

    private void CalculateScreenPosition()
    {
        RectTransform CanvasRect = Canvas.GetComponent<RectTransform>();

        //then you calculate the position of the UI element
        //0,0 for the canvas is at the center of the screen, whereas WorldToViewPortPoint treats the lower left corner as 0,0. Because of this, you need to subtract the height / width of the canvas * 0.5 to get the correct position.

        Vector2 ViewportPosition = Camera.WorldToViewportPoint(InspectableObj.transform.position);
        Vector2 WorldObject_ScreenPosition = new Vector2(
        ((ViewportPosition.x * CanvasRect.sizeDelta.x) - (CanvasRect.sizeDelta.x * 0.5f)),
        ((ViewportPosition.y * CanvasRect.sizeDelta.y) - (CanvasRect.sizeDelta.y * 0.5f)));

        //now you can set the position of the ui element
        Textbox.anchoredPosition = WorldObject_ScreenPosition  + TextBoxOffset;
        Button.anchoredPosition = WorldObject_ScreenPosition  + ButtonOffset;
    }

    public void Enter(bool value)
    {
        CalculateScreenPosition();
        spriteRenderer.sprite = highlightedSprite;

        if (value == true)
        {
            Button.gameObject.SetActive(true);
            if (Input.GetKeyDown(toggleKey))
            {
                // Cambia la visibilit� dello sprite e della TextBox
                Button.gameObject.SetActive(false);
                Textbox.gameObject.SetActive(true);              
            }
            else if (value == false)
            {
                Textbox.gameObject.SetActive(false);
                Button.gameObject.SetActive(false);
            }

            if (value == true && Textbox.gameObject.activeSelf == true)
            {
                Button.gameObject.SetActive(false);
            }
        }
    }

    public void TextExit(bool value)
    {
        if (value == true)
        {
            Textbox.gameObject.SetActive(false);
        }

        spriteRenderer.sprite = defaultSprite;
    }


    public void ButtonExit(bool value)
    {
        if (value == true)
        {
            Button.gameObject.SetActive(false);
        }
    }
}
