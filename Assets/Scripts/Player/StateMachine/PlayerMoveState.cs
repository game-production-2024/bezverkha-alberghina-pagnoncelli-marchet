using UnityEngine;

public class PlayerMoveState : PlayerStateBase
{
    public PlayerMoveState(PlayerStateController playerStateController) : base(playerStateController)
    {
    }

    public override void EnterState()
    {
        Debug.Log("Benvenuto nel Move State");
        player.animator.SetBool("Walk", true);

        if (player.StaminaCounter != player.maxStamina)
        {
            player.StaminaCounter = player.maxStamina;
        }
        AudioManager.Instance.PlayLoopSFX(AudioManager.Instance.run);
    }

    public override void UpdateState()
    {
        // Implementazione del raycasting per rilevare i muri
        DetectWallCollision(IsNearWall());

        float moveInput = Input.GetAxis("Horizontal");

        // Esci dalla funzione se non c'� input
        if (moveInput == 0)
        {
            return;
        }

        // Gestisce la direzione in cui il giocatore sta guardando
        if (Input.GetKeyDown(player.rightKey) || moveInput > 0)
        {
            player.isFacingRight = true;
            player.RotatePlayer(true);
        }
        else if (Input.GetKeyDown(player.leftKey) || moveInput < 0)
        {
            player.isFacingRight = false;
            player.RotatePlayer(false);
        }

        if (!Input.GetKey(player.rightKey) && !Input.GetKey(player.leftKey))
        {
            player.SetState(new PlayerIdleState(player));
        }

        if (!player.isGrounded || !player.CanUseCoyoteTime())
        {
            player.SetState(new PlayerFallingState(player));
        }

        if (player.jumpsAvailable > 0 && Input.GetKeyDown(player.jumpKey) && (player.isGrounded || player.CanUseCoyoteTime()))
        {
            player.SetState(new PlayerJumpState(player));
        }

        if (IsNearWall() && Input.GetKeyDown(player.jumpKey)) // Utilizzo di IsNearWall
        {
            player.SetState(new PlayerJumpState(player));
        }

        if (IsNearWall() && player.StaminaCounter > 0 && Input.GetKey(player.climbKey)) // Utilizzo di IsNearWall
        {
            player.SetState(new PlayerClimbState(player));
        }
        if (IsNearWall() && player.StaminaCounter > 0 && Input.GetKey(player.climbKeyAlt)) // Utilizzo di IsNearWall
        {
            player.SetState(new PlayerClimbState(player));
        }

        if (player.canDash == true && Input.GetKeyDown(player.dashKey))
        {
            player.SetState(new PlayerDashState(player));
        }

        if (Input.GetKeyDown(player.ipdKey) && player.canUseIPD == true)
        {
            player.IPD();
        }
        if (Input.GetKeyDown(player.ipdKeyAlt) && player.canUseIPD == true)
        {
            player.IPD();
        }

        if (Input.GetKeyDown(player.deathKey))
        { 
            player.Death();
        }
    }

    public override void FixedUpdateState()
    {
        float moveInput = Input.GetAxis("Horizontal");
        player.MovePlayer(moveInput);

        // Controlla se il giocatore non sta arrampicando e non sta eseguendo uno scatto
        if (!player.isClimbing && !player.IsDashing)
        {

            // Esci dalla funzione se non c'� input
            if (moveInput == 0)
            {
                return;
            }

            // Muovi il giocatore orizzontalmente
            player.MovePlayer(moveInput);

            // Controlla la collisione con il muro
            DetectWallCollision(IsNearWall());
        }
    }

    public override void ExitState()
    {
        player.animator.SetBool("Walk", false);
        AudioManager.Instance.StopLoopSFX(AudioManager.Instance.run);
    }

    // Metodo per rilevare la collisione con il muro
    private void DetectWallCollision(bool value)
    {
        player.DetectWallCollision(value);
    }

    // Metodo per determinare se il giocatore � vicino a un muro
    private bool IsNearWall()
    {
        return player.IsNearWall();
    }
}
