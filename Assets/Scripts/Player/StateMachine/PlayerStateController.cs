using UnityEngine;
using System.Collections;

// Enumerazione che rappresenta gli stati del giocatore
public enum PlayerState
{
    Idle,    // Stato di riposo
    Move,    // Stato di movimento
    Jump,    // Stato di salto
    Climb,   // Stato di arrampicata
    Dash,    // Stato di scatto
    Glide    // Stato di planata
}

public class PlayerStateController : MonoBehaviour
{
    public GameObject trail;
    public GameObject staminaObject;
    public GameObject deathParticlePref;
    public Transform currentCollidingWall;
    public Transform currentCollidingFloor;
    public Transform currentMovingPlatform;       // Piattaforma mobile attuale
    public Transform currentFallingPlatform;      // Piattaforma cadente attuale
    public Vector3 platformRelativePosition;     // Posizione relativa del giocatore sulla piattaforma
    private Vector2 fallingPlatformVelocity;
    public bool isPlayerMoving;                  // Indica se il giocatore   in movimento
    public float nearWallThreshold = 0.1f;

    // Variabili per i parametri del giocatore
    public float moveSpeed = 4f;                 // Velocit  di movimento
    public float walkSpeed = 4f;                  // Velocit  di camminata
    public float jumpForce = 10f;                 // Forza del salto
    public float fallSpeed = 10f;                 // Velocit  di caduta
    public float wallJumpForce = 15f;                 // Forza del salto per il wallJump
    public float wallJumpHorizontalForce = 2f;
    public float wallJumpVerticalForce = 8f;
    public int jumpsAvailable = 1;                // Numero di salti disponibili per il giocatore
    public int jumpsMaximum = 1;                  // Numero massimo di salti
    public float climbSpeed = 5f;                 // Velocit  di arrampicata
    public float dashDistance = 5f;               // Distanza dello scatto
    public float dashDuration = 0.2f;             // Durata dello scatto
    public float staminaCounter = 20f;            // Contatore della stamina
    public float staminaDrainRate = 1f;           // Velocit  di consumo della stamina
    public float staminaDrainInterval = 1f;       // Intervallo di tempo per il consumo della stamina
    public float maxStamina = 20f;                // Valore massimo di stamina
    public float originalGravityScale; // Memorizza la gravit  originale
    public float gravityModifier = 0.5f;
    public float coyoteTimeDuration = 0.2f; // Durata del Coyote Time in secondi
    public float coyoteTimeCounter;
    public float disableTime = 2f;
    public float ipdUseTimeMax = 6f;
    public float ipdUseTimer = 6f;
    public float ipdCdTimeMax = 3f;
    public float ipdCdTimer = 3f;

    // Tasti di input
    public KeyCode rightKey = KeyCode.D;          // Tasto per muoversi a destra
    public KeyCode leftKey = KeyCode.A;           // Tasto per muoversi a sinistra
    public KeyCode jumpKey = KeyCode.Space;       // Tasto di salto
    public KeyCode upclimbKey = KeyCode.W;        // Tasto di arrampicata verso l'alto
    public KeyCode downclimbKey = KeyCode.S;      // Tasto di arrampicata verso il basso
    public KeyCode dashKey = KeyCode.LeftShift;   // Tasto di scatto
    public KeyCode climbKey = KeyCode.Mouse1;     // Tasto per l'aggancio a muro
    public KeyCode climbKeyAlt = KeyCode.K;       // Tasto per l'aggancio a muro alternativo
    public KeyCode ipdKey = KeyCode.J;            // Tasto per l'attivazione del IPD
    public KeyCode ipdKeyAlt = KeyCode.Q;         // Tasto per l'attivazione del IPD alternativa
    public KeyCode deathKey = KeyCode.T;          // Tasto per l'attivazione morte manuale, da usare per i softlocks

    // Riferimenti ai componenti del giocatore
    public Rigidbody2D rb;                        // Riferimento al componente Rigidbody2D
    public Animator animator;                     // Riferimento al componente Animator
    private GameObject floorCollider;

    // Variabili di stato del giocatore
    public bool onDialogue = false;
    public bool dashUnlock = false;
    public bool ipdUnlock = false;
    public bool isFacingRight = true;
    public bool isWallJumping = false;
    public bool isGrounded = false;                // Indica se il giocatore   a terra
    public bool nearWall = false;
    public bool isClimbing = false;               // Indica lo stato di arrampicata
    public bool canDash = true;                   // Indica la disponibilit  dello scatto
    public bool canUseIPD = true;
    public bool ipdTimerActive = false;
    public bool ipdActive = false;                // Indica se l'IPD � attivo
    public bool ipdCD = false;
    private bool ipdSoundPlayed = false;

    private TrailRenderer trailRenderer;
    private StaminaBar staminaBar;

    [SerializeField] private bool isDashing = false;                // Indica lo stato di scatto

    public float StaminaCounter
    {
        get => staminaCounter;
        set
        {
            staminaBar.SetStamina(value);
            staminaCounter = value;
        }
    }

    public bool IsDashing
    {
        get => isDashing;
        set
        {
            trailRenderer.Clear();
            trail.SetActive(value);
            isDashing = value;
        }
    }

    public float staminaDrainTimer = 0f;          // Timer per il consumo della stamina

    public bool isWallJumpOnCooldown = false;  // Indica se il wall jump � in cooldown
    public float wallJumpCooldownTime = 2.0f;  // Tempo di cooldown in secondi
    public float wallJumpCooldownTimer;

    public PlayerStateBase currentState;          // Stato attuale del giocatore

    void Awake()
    {
        // Ottieni i riferimenti ai componenti
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        trailRenderer = trail.GetComponent<TrailRenderer>();
        staminaBar = staminaObject.GetComponent<StaminaBar>();
        staminaCounter = maxStamina;
        staminaBar.SetStamina(StaminaCounter);

        // Imposta lo stato iniziale del giocatore
        currentState = new PlayerIdleState(this);
    }

    void Update()
    {
        currentState?.UpdateState();

        if (isGrounded)
        {
            coyoteTimeCounter = coyoteTimeDuration;
        }
        else
        {
            coyoteTimeCounter -= Time.deltaTime;
        }

        if ((StaminaCounter != maxStamina) && isGrounded == true)
        {
            StaminaCounter = maxStamina;
        }

        if (ipdTimerActive)
        {
            // Decrementa il timer di utilizzo, sottraendo Time.deltaTime per scalare con il tempo reale
            if (ipdUseTimer > 0)
            {
                ipdUseTimer -= Time.deltaTime;  // Diminuire in modo fluido
            }
            else if (ipdUseTimer <= 0 && !ipdSoundPlayed)  // Verifica se il suono è stato già riprodotto
            {
                ipdUseTimer = 0;
                ipdActive = false;
                AudioManager.Instance.PlaySFX(AudioManager.Instance.ipdoff);
                ipdSoundPlayed = true;  // Imposta la variabile a true per evitare di rigiocare l'audio
            }

            // Gestione del cooldown quando l'IPD è disattivato
            if (!ipdActive)
            {
                // Decrementiamo il timer di cooldown
                ipdCdTimer -= Time.deltaTime;

                if (ipdCdTimer <= 0)
                {
                    // Ripristina il timer di utilizzo e abilita nuovamente l'uso dell'IPD
                    ipdUseTimer = ipdUseTimeMax;
                    canUseIPD = true;  // Riabilitiamo l'uso dell'IPD
                    ipdCdTimer = ipdCdTimeMax;  // Ripristiniamo il timer di cooldown
                    ipdTimerActive = false;     // Disabilitiamo il timer di utilizzo
                }
            }
        }

        if (isWallJumpOnCooldown)
        {
            wallJumpCooldownTimer -= Time.deltaTime;  // Decrementa il timer

            // Se il cooldown � finito, abilita di nuovo il wall jump
            if (wallJumpCooldownTimer <= 0)
            {
                isWallJumpOnCooldown = false;
            }
        }

        // Gestisci l'input per il dash
        if (Input.GetKeyDown(dashKey) && canDash)
        {
            Vector3 wallPos = WallPosition();
            if (wallPos != Vector3.zero)
            {
                currentState = new PlayerDashState(this);
                currentState.EnterState();
            }
        }

        // Controlla se il giocatore sta muovendo o arrampicando
        if (Input.GetKey(rightKey) || Input.GetKey(leftKey) || Input.GetKey(dashKey) || isClimbing)
        {
            isPlayerMoving = true;
        }
        else
        {
            isPlayerMoving = false;
        }

        // Se il giocatore su una piattaforma mobile, aggiorna la sua posizione
        if (currentMovingPlatform != null)
        {
            // Aggiorna la posizione del giocatore in base alla posizione della piattaforma e alla posizione relativa
            transform.position = currentMovingPlatform.position + platformRelativePosition;
        }
        // Se il giocatore   su una piattaforma cadente, aggiorna la sua posizione solo sull'asse Y
        if (currentFallingPlatform != null)
        {
            // Aggiorna la posizione del giocatore in base alla posizione della piattaforma e alla posizione relativa
            fallingPlatformVelocity = currentFallingPlatform.GetComponent<Rigidbody2D>().velocity;
            transform.position = new Vector3(transform.position.x, currentFallingPlatform.position.y + platformRelativePosition.y, transform.position.z);
        }
    }

    public bool IsNearWall()
    {
        return nearWall;
    }

    void FixedUpdate()
    {
        // Esegui l'aggiornamento dello stato attuale del giocatore
        currentState?.FixedUpdateState();
    }


    public void SetState(PlayerStateBase newState)
    {
        // Uscire dallo stato attuale
        currentState.ExitState();

        // Imposta il nuovo stato
        currentState = newState;

        // Entra nello stato nuovo
        currentState.EnterState();
    }

    public void DetectFloorCollision(bool value)
    {
        // Imposta lo stato del giocatore a terra
        isGrounded = value;

        // Reimposta i salti disponibili quando il giocatore tocca il suolo
        if (value == true)
        {
            ResetJumps();
            rb.velocity = Vector2.zero; // Player doesn't slide on the ground after diagonal fall

            // Esci dalla modalit  arrampicata quando il giocatore tocca il suolo
            if (isClimbing)
            {
                isClimbing = false;
            }
        }
    }

    public void DetectWallCollision(bool value)
    {
        nearWall = value;

        // Reimposta i salti disponibili quando il giocatore tocca il muro
        if (nearWall == true || isGrounded == true)
        {
            ResetJumps();
            return;
        }

        jumpsAvailable = 0;
    }

    public Vector3 WallPosition()
    {
        // Crea un raycast per rilevare il muro
        RaycastHit2D hit = Physics2D.Raycast(transform.position, isFacingRight ? Vector2.right : Vector2.left, 1f);
        if (hit.collider != null && (hit.collider.CompareTag("R_Wall") || hit.collider.CompareTag("L_Wall") || hit.collider.CompareTag("Wall")))
        {
            return hit.point;
        }
        return Vector3.zero;
    }

    public void MovePlayer(float moveInput)
    {
        if (isWallJumpOnCooldown)
        { 
            return;
        }

        // Se il giocatore ha eseguito un wall jump
        if (isWallJumping)
        {
            // Se il giocatore sta tentando di muoversi a destra o sinistra dopo il wall jump
            if (moveInput != 0)
            {
                // Imposta la velocit� orizzontale direttamente in base alla direzione del movimento
                float newVelocityX = moveInput * wallJumpHorizontalForce;
                rb.velocity = new Vector2(newVelocityX, rb.velocity.y);

                // Considera il wall jump completato dopo che il movimento laterale � iniziato
                isWallJumping = false;

                if (isWallJumping == false)
                {
                    rb.velocity = Vector3.zero;
                }
            }
        }
        else
        {
            // Muovi il giocatore orizzontalmente normalmente
            transform.Translate(transform.right * moveInput * moveSpeed * Time.deltaTime);
        }

        // Movimento verticale
        if (!isGrounded && rb.velocity.y < 0)
        {
            // Imposta la velocit� di caduta
            // moveSpeed = fallSpeed;
        }
        else
        {
            moveSpeed = walkSpeed; // Imposta la velocit� di camminata
        }

        // Aggiorna la posizione relativa del giocatore sulla piattaforma mobile
        if (currentMovingPlatform != null)
        {
            platformRelativePosition = transform.position - currentMovingPlatform.position;
        }

        // Aggiorna la posizione relativa del giocatore sulla piattaforma cadente
        if (currentFallingPlatform != null)
        {
            platformRelativePosition = new Vector3(0, transform.position.y - currentFallingPlatform.position.y, 0);
        }
    }

    public void RotatePlayer(bool isFacingRight)
    {
        if (isWallJumpOnCooldown)
        {
            return;
        }

        if (isFacingRight)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        else
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
    }

    public void ResetJumps()
    {
        jumpsAvailable = jumpsMaximum;
    }
    public void PrepareForJump()
    {
        originalGravityScale = rb.gravityScale; // Memorizza la gravit  attuale
    }

    public void RestoreGravity()
    {
        rb.gravityScale = originalGravityScale; // Ripristina la gravit  memorizzata
    }

    public bool CanUseCoyoteTime()
    {
        return coyoteTimeCounter > 0f;
    }

    public void IPD()
    {
        if (ipdUnlock == false)
        {
            Debug.LogWarning("IPD non sbloccato");
            return;
        }

        if (ipdCD == true || !canUseIPD)  // Aggiungiamo controllo su canUseIPD
        {
            Debug.LogWarning("Invisible Platform Detector in ricarica o non utilizzabile");
            return;
        }

        AudioManager.Instance.PlaySFX(AudioManager.Instance.ipdon);
        ipdActive = true;
        canUseIPD = false;
        ipdTimerActive = true;
        ipdCdTimer = ipdCdTimeMax;  // Assicuriamoci che il timer di cooldown venga ripristinato
        ipdSoundPlayed = false;     // Resettiamo lo stato del suono di disattivazione
    }


    public void Death()
    {
        if (onDialogue == true)
        {
            return;
        }

        gameObject.SetActive(false);
        AudioManager.Instance.PlaySFX(AudioManager.Instance.death);
        GameManager.Instance.GameOver(true);
        Instantiate(deathParticlePref, gameObject.transform.position, gameObject.transform.rotation);
    }
    public bool IsMovingLeft()
    {
        // Verifica se il tasto di movimento sinistro è premuto
        return Input.GetKey(leftKey);
    }

    public bool IsMovingRight()
    {
        // Verifica se il tasto di movimento destro è premuto
        return Input.GetKey(rightKey);
    }



    public void OnCollisionEnter2D(Collision2D collision)
    {
        // Se il giocatore entra in collisione con un muro destro o sinistro
        if (collision.gameObject.CompareTag("MovingPlatform") || collision.gameObject.CompareTag("FallingPlatform"))
        {
            currentCollidingWall = collision.transform;

            // Se il giocatore ha abbastanza stamina, entra nello stato di arrampicata
            if (IsNearWall() && StaminaCounter > 0 && Input.GetKey(climbKey))
            {
                SetState(new PlayerClimbState(this));
                rb.gravityScale = 0; // Imposta la gravit  a 0 durante l'arrampicata
            }

            if (IsNearWall() && StaminaCounter > 0 && Input.GetKey(climbKeyAlt))
            {
                SetState(new PlayerClimbState(this));
                rb.gravityScale = 0; // Imposta la gravit  a 0 durante l'arrampicata
            }
        }
    }

    public void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("MovingPlatform"))
        {
            currentMovingPlatform = collider.transform;
            platformRelativePosition = transform.position - currentMovingPlatform.position;
        }
        // Se il giocatore entra in collisione con una piattaforma cadente
        else if (collider.gameObject.CompareTag("FallingPlatform") || collider.gameObject.CompareTag("MovingPlatform"))
        {
            currentFallingPlatform = collider.transform;
            platformRelativePosition = transform.position - currentFallingPlatform.position;
            if (rb.velocity.y < 0)
            {
                isGrounded = true;
            }
        }
    }

    public void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform == currentCollidingWall)
        {
            currentCollidingWall = null;
        }

        // Se il giocatore esce dalla collisione con un muro destro o sinistro, riportalo allo stato di riposo
        if (collision.gameObject.CompareTag("MovingPlatform") || collision.gameObject.CompareTag("FallingPlatform") && rb.velocity.y == 0)
        {
            rb.gravityScale = 2; // Reimposta la gravit  a 1
        }
    }

    public void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.transform == currentCollidingWall)
        {
            currentCollidingWall = null;
        }
        if (collider.transform == currentCollidingFloor)
        {
            currentCollidingFloor = null;
        }
        // Se il giocatore esce dalla collisione con una piattaforma mobile
        else if (collider.gameObject.CompareTag("MovingPlatform"))
        {
            currentMovingPlatform = null;
            platformRelativePosition = Vector2.zero; // Resetta la posizione relativa
        }
        // Se il giocatore esce dalla collisione con una piattaforma cadente
        else if (collider.gameObject.CompareTag("FallingPlatform"))
        {
            currentFallingPlatform = null;
            platformRelativePosition = Vector2.zero; // Resetta la posizione relativa
            isGrounded = false; // Resetta isGrounded quando esce dalla piattaforma cadente
        }
    }
}