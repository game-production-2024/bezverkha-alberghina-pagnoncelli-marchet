using UnityEngine;
public abstract class PlayerStateBase : MonoBehaviour
{
    protected PlayerStateController player;

    public PlayerStateBase(PlayerStateController playerStateController)
    {
        player = playerStateController;
    }

    public virtual void EnterState() { }
    public virtual void UpdateState() { }
    public virtual void FixedUpdateState() { }
    public virtual void ExitState() { }
}