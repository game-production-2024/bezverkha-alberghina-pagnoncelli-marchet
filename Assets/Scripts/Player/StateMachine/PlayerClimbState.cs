using UnityEngine;

public class PlayerClimbState : PlayerStateBase
{
    private float originalVelocityX; // Memorizza la velocità originale sull'asse X
    private Transform currentMovingPlatform; // Memorizza la piattaforma in movimento
    private Vector3 platformStartPos; // Memorizza la posizione iniziale della piattaforma
    private Vector3 playerStartPos; // Memorizza la posizione iniziale del giocatore relativa alla piattaforma

    private bool isMovementLocked;  // Indica se il movimento opposto è bloccato
    private float lockDuration = 0.5f;  // Durata del blocco (mezzo secondo)
    private float lockTimer;  // Timer per gestire la durata del blocco
    private bool isClimbIdleSoundPlaying = false;

    public PlayerClimbState(PlayerStateController playerStateController) : base(playerStateController)
    {
    }

    public override void EnterState()
    {
        Debug.Log("Benvenuto nel Climb State");
        player.isClimbing = true;
        player.originalGravityScale = player.rb.gravityScale; // Memorizza la gravità originale
        player.rb.gravityScale = 0; // Setta la gravità a 0 quando entra nello stato di arrampicata
        originalVelocityX = player.rb.velocity.x; // Memorizza la velocità originale sull'asse X
        player.rb.velocity = new Vector2(0, player.rb.velocity.y); // Imposta la velocità sull'asse X a 0

        // Controlla se il giocatore è agganciato a una piattaforma in movimento
        if (player.currentCollidingWall != null && player.currentCollidingWall.CompareTag("MovingPlatform"))
        {
            currentMovingPlatform = player.currentCollidingWall;
            platformStartPos = currentMovingPlatform.position; // Memorizza la posizione iniziale della piattaforma
            playerStartPos = player.transform.position - currentMovingPlatform.position; // Memorizza la posizione relativa del giocatore rispetto alla piattaforma
        }
        else
        {
            currentMovingPlatform = null;
        }

        UpdateAnimationState();
    }

    public override void UpdateState()
    {
        if (isMovementLocked)
        {
            lockTimer -= Time.deltaTime;
            if (lockTimer <= 0)
            {
                isMovementLocked = false;  // Sblocca il movimento dopo il tempo specificato
            }
        }

        // Verifica se il giocatore è ancora vicino al muro, considerando anche la piattaforma mobile
        if (IsNearWall() && player.staminaCounter > 0 && Input.GetKey(player.climbKey))
        {
            player.animator.SetBool("Climb", true);
            player.animator.SetBool("Climb_Idle", false);
            Climb();
        }
        else if (IsNearWall() && player.staminaCounter > 0 && Input.GetKey(player.climbKeyAlt))
        {
            player.animator.SetBool("Climb", true);
            player.animator.SetBool("Climb_Idle", false);
            Climb();
        }
        else
        {
            player.SetState(new PlayerFallingState(player));
        }

        if (Input.GetKey(player.upclimbKey) == false && Input.GetKey(player.downclimbKey) == false)
        {
            player.animator.SetBool("Climb", false);
            player.animator.SetBool("Climb_Idle", true);

            // Fermiamo il suono del climb se era in loop
            AudioManager.Instance.StopLoopSFX(AudioManager.Instance.climb);

            // Se il suono climbidle non è già in esecuzione, riproducilo
            if (!isClimbIdleSoundPlaying)
            {
                AudioManager.Instance.PlaySFX(AudioManager.Instance.climbidle);
                isClimbIdleSoundPlaying = true; // Imposta il flag a true
            }
        }
        else
        {
            // Quando il giocatore ricomincia a muoversi, resetta il flag
            isClimbIdleSoundPlaying = false;
        }

        if (Input.GetKeyUp(player.climbKey) && player.isGrounded == false)
        {
            player.SetState(new PlayerFallingState(player));
        }

        if (Input.GetKeyUp(player.climbKey) && player.isGrounded == true)
        {
            player.SetState(new PlayerIdleState(player));
        }

        if (Input.GetKeyDown(player.ipdKey) && player.canUseIPD == true)
        {
            player.IPD();
        }
        if (Input.GetKeyDown(player.ipdKeyAlt) && player.canUseIPD == true)
        {
            player.IPD();
        }

        if (Input.GetKeyDown(player.deathKey))
        {
            player.Death();
        }
    }

    public override void FixedUpdateState()
    {
        // Aggiorna la posizione del giocatore in base alla piattaforma in movimento
        if (currentMovingPlatform != null)
        {
            Vector3 platformMovement = currentMovingPlatform.position - platformStartPos;
            player.transform.position = currentMovingPlatform.position + playerStartPos; // Mantieni la posizione relativa del giocatore alla piattaforma
            platformStartPos = currentMovingPlatform.position; // Aggiorna la posizione iniziale per il prossimo frame
        }
    }

    public override void ExitState()
    {
        player.isClimbing = false;
        player.animator.SetBool("Climb", false);
        player.animator.SetBool("Climb_Idle", false);
        player.rb.gravityScale = player.originalGravityScale; // Ripristina la gravità originale quando esce dallo stato di arrampicata
        player.rb.velocity = new Vector2(originalVelocityX, player.rb.velocity.y); // Ripristina la velocità originale sull'asse X

        AudioManager.Instance.StopLoopSFX(AudioManager.Instance.climb);

        // Sgancia il giocatore dalla piattaforma in movimento
        currentMovingPlatform = null;
    }

    public void Climb()
    {
        if (player.StaminaCounter < 1)
        {
            player.SetState(new PlayerFallingState(player));
            return;
        }

        // Definisce la velocità verticale in base ai tasti premuti per arrampicarsi verso l'alto o verso il basso
        float vertical = 0;
        if (Input.GetKey(player.upclimbKey) && player.StaminaCounter != 0)
        {
            vertical = player.climbSpeed;
        }
        else if (Input.GetKey(player.downclimbKey) && player.StaminaCounter != 0)
        {
            vertical = -player.climbSpeed;
        }

        if (Input.GetKeyDown(player.jumpKey))
        {
            WallJump();
            return; // Esce dalla funzione Climb() ed esegue WallJump()
        }

        // Aggiorna il timer di consumo della stamina
        player.staminaDrainTimer += Time.deltaTime;
        if (player.staminaDrainTimer >= player.staminaDrainInterval)
        {
            player.StaminaCounter -= player.staminaDrainRate;
            player.staminaDrainTimer = 0f;
        }

        // Se la stamina è esaurita, esci dalla modalità arrampicata
        if (player.StaminaCounter < 1 && player.isGrounded == false)
        {
            player.StaminaCounter = 0;
            player.SetState(new PlayerFallingState(player));
        }

        // Imposta la velocità verticale del giocatore
        player.rb.velocity = new Vector2(0, vertical); // Imposta la velocità sull'asse X a 0

        // Aggiorna la posizione relativa del giocatore sulla piattaforma mobile
        if (currentMovingPlatform != null)
        {
            player.platformRelativePosition = player.transform.position - currentMovingPlatform.position;
        }
        AudioManager.Instance.PlayLoopSFX(AudioManager.Instance.climb);
    }

    // Metodo per determinare se il giocatore è vicino a un muro
    public bool IsNearWall()
    {
        // Aggiungi un controllo per verificare se il giocatore è vicino a una piattaforma in movimento
        if (currentMovingPlatform != null)
        {
            float distanceToPlatform = Vector2.Distance(player.transform.position, currentMovingPlatform.position);
            return distanceToPlatform <= player.nearWallThreshold;
        }
        return player.IsNearWall();
    }

    private void UpdateAnimationState()
    {
        if (Input.GetKey(player.upclimbKey) == false && Input.GetKey(player.downclimbKey) == false)
        {
            player.animator.SetBool("Climb", false);
            player.animator.SetBool("Climb_Idle", true);
        }
        else
        {
            player.animator.SetBool("Climb", true);
            player.animator.SetBool("Climb_Idle", false);
        }
    }

    public void WallJump()
    {
        // Controlla se il wall jump è in cooldown
        if (player.isWallJumpOnCooldown)
        {
            return;  // Esci dalla funzione se il wall jump è ancora in cooldown
        }

        player.SetState(new PlayerFallingState(player));

        // Esegui il wall jump
        player.isWallJumping = true;

        // Disabilita la gravità per consentire il wall jump
        player.rb.gravityScale = player.originalGravityScale;

        // Calcola la direzione del wall jump con un'angolazione di 30 gradi
        float wallJumpAngle = 30f;

        // Blocca il movimento opposto
        isMovementLocked = true;
        lockTimer = lockDuration;

        if (player.isFacingRight)
        {
            // Se il giocatore sta guardando a destra, il wall jump lo spinge a sinistra
            Vector2 wallJumpDirection = Quaternion.Euler(0, 0, wallJumpAngle) * Vector2.left * player.wallJumpHorizontalForce;
            wallJumpDirection += Vector2.up * player.wallJumpVerticalForce;  // Aggiungi forza verso l'alto
            player.rb.velocity = wallJumpDirection.normalized * player.wallJumpForce;

            // Ruota il giocatore a sinistra
            player.RotatePlayer(false);
            player.isFacingRight = false;  // Imposta il giocatore a guardare a sinistra
        }
        else
        {
            // Se il giocatore sta guardando a sinistra, il wall jump lo spinge a destra
            Vector2 wallJumpDirection = Quaternion.Euler(0, 0, -wallJumpAngle) * Vector2.right * player.wallJumpHorizontalForce;
            wallJumpDirection += Vector2.up * player.wallJumpVerticalForce;  // Aggiungi forza verso l'alto
            player.rb.velocity = wallJumpDirection.normalized * player.wallJumpForce;

            // Ruota il giocatore a destra
            player.RotatePlayer(true);
            player.isFacingRight = true;  // Imposta il giocatore a guardare a destra
        }

        // Decrementa il numero di salti disponibili
        player.jumpsAvailable--;
        player.animator.SetTrigger("Jump");

        // Imposta il cooldown del wall jump
        player.isWallJumpOnCooldown = true;
        player.wallJumpCooldownTimer = player.wallJumpCooldownTime;  // Inizia il timer di cooldown
        AudioManager.Instance.PlaySFX(AudioManager.Instance.jump);
    }
}
