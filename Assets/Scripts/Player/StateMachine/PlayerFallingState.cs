using UnityEngine;

public class PlayerFallingState : PlayerStateBase
{
    private float originalVelocityX; // Memorizza la velocità originale sull'asse X

    private bool isMovementLocked;  // Indica se il movimento opposto è bloccato
    private float lockDuration = 0.5f;  // Durata del blocco (mezzo secondo)
    private float lockTimer;  // Timer per gestire la durata del blocco
    public float moveInput;

    public PlayerFallingState(PlayerStateController playerStateController) : base(playerStateController)
    {
    }

    public override void EnterState()
    {
        Debug.Log("Benvenuto nel Falling State");
        player.PrepareForJump();
        player.animator.SetBool("Fall", true);
        player.rb.gravityScale = player.originalGravityScale + player.gravityModifier;
    }

    public override void UpdateState()
    {
        if (isMovementLocked)
        {
            lockTimer -= Time.deltaTime;
            if (lockTimer <= 0)
            {
                isMovementLocked = false;  // Sblocca il movimento dopo il tempo specificato
            }
        }

        // Implementazione del raycasting per rilevare i muri
        DetectWallCollision(IsNearWall());

        if (player.isGrounded == true && (player.currentFallingPlatform == false || player.currentMovingPlatform == false))
        {
            player.SetState(new PlayerIdleState(player));
            AudioManager.Instance.PlaySFX(AudioManager.Instance.landing);
        }

        if (IsNearWall() && Input.GetKey(player.climbKey) && player.StaminaCounter > 0 && player.isWallJumping == false) // Utilizzo di IsNearWall
        {
            player.SetState(new PlayerClimbState(player));
        }
        if (IsNearWall() && Input.GetKey(player.climbKeyAlt) && player.StaminaCounter > 0 && player.isWallJumping == false) // Utilizzo di IsNearWall
        {
            player.SetState(new PlayerClimbState(player));
        }

        if (player.canDash == true && Input.GetKeyDown(player.dashKey))
        {
            player.SetState(new PlayerDashState(player));
        }
        else if (player.nearWall && Input.GetKeyDown(player.jumpKey))
        {
            WallJump();
        }

        if (Input.GetKeyDown(player.ipdKey) && player.canUseIPD == true)
        {
            player.IPD();
        }
        if (Input.GetKeyDown(player.ipdKeyAlt) && player.canUseIPD == true)
        {
            player.IPD();
        }

        if (Input.GetKeyDown(player.deathKey))
        {
            player.Death();
        }
    }

    public override void FixedUpdateState()
    {
        moveInput = Input.GetAxis("Horizontal");

        // Se il movimento è bloccato, impedisci il movimento nella direzione opposta
        if (isMovementLocked)
        {
            // Se il giocatore è rivolto a destra, impedisci di muoversi a sinistra
            if (!player.isFacingRight && moveInput < 0)
            {
                moveInput = 0;  // Blocca il movimento verso sinistra
            }
            // Se il giocatore è rivolto a sinistra, impedisci di muoversi a destra
            else if (player.isFacingRight && moveInput > 0)
            {
                moveInput = 0;  // Blocca il movimento verso destra
            }
        }

        // Muovi il giocatore orizzontalmente normalmente se non è bloccato
        player.MovePlayer(moveInput);

        if (IsNearWall())
        {
            return;
        }

        // Controlla se il giocatore non sta arrampicando e non sta eseguendo uno scatto
        if (!player.isClimbing && !player.IsDashing)
        {
            // Gestisce la direzione in cui il giocatore sta guardando
            if (Input.GetKeyDown(player.rightKey) || moveInput > 0)
            {
                player.isFacingRight = true;
                player.RotatePlayer(true);
            }
            else if (Input.GetKeyDown(player.leftKey) || moveInput < 0)
            {
                player.isFacingRight = false;
                player.RotatePlayer(false);
            }

            // Muovi il giocatore orizzontalmente
            player.MovePlayer(moveInput);

            // Controlla la collisione con il muro
            DetectWallCollision(IsNearWall());
        }
    }

    public override void ExitState()
    {
        player.animator.SetBool("Fall", false);
        player.RestoreGravity(); // Assicura che la gravit  venga ripristinata quando si esce dallo stato
    }

    public void WallJump()
    {
        // Controlla se il wall jump è in cooldown
        if (player.isWallJumpOnCooldown)
        {
            return;  // Esci dalla funzione se il wall jump è ancora in cooldown
        }

        moveInput = 0;

        // Esegui il wall jump
        player.isWallJumping = true;

        // Disabilita la gravità per consentire il wall jump
        player.rb.gravityScale = player.originalGravityScale;

        // Calcola la direzione del wall jump con un'angolazione di 30 gradi
        float wallJumpAngle = 30f;

        // Blocca il movimento opposto
        isMovementLocked = true;
        lockTimer = lockDuration;

        if (player.isFacingRight)
        {
            // Se il giocatore sta guardando a destra, il wall jump lo spinge a sinistra
            Vector2 wallJumpDirection = Quaternion.Euler(0, 0, wallJumpAngle) * Vector2.left * player.wallJumpHorizontalForce;
            wallJumpDirection += Vector2.up * player.wallJumpVerticalForce;  // Aggiungi forza verso l'alto
            player.rb.velocity = wallJumpDirection.normalized * player.wallJumpForce;

            // Ruota il giocatore a sinistra
            player.RotatePlayer(false);
            player.isFacingRight = false;  // Imposta il giocatore a guardare a sinistra
        }
        else
        {
            // Se il giocatore sta guardando a sinistra, il wall jump lo spinge a destra
            Vector2 wallJumpDirection = Quaternion.Euler(0, 0, -wallJumpAngle) * Vector2.right * player.wallJumpHorizontalForce;
            wallJumpDirection += Vector2.up * player.wallJumpVerticalForce;  // Aggiungi forza verso l'alto
            player.rb.velocity = wallJumpDirection.normalized * player.wallJumpForce;

            // Ruota il giocatore a destra
            player.RotatePlayer(true);
            player.isFacingRight = true;  // Imposta il giocatore a guardare a destra
        }

        // Decrementa il numero di salti disponibili
        player.jumpsAvailable--;
        player.animator.SetTrigger("Jump");

        // Imposta il cooldown del wall jump
        player.isWallJumpOnCooldown = true;
        player.wallJumpCooldownTimer = player.wallJumpCooldownTime;  // Inizia il timer di cooldown
        AudioManager.Instance.PlaySFX(AudioManager.Instance.jump);
    }


    // Metodo per rilevare la collisione con il muro
    private void DetectWallCollision(bool value)
    {
        player.DetectWallCollision(value);
    }

    private void DetectFloorCollision(bool value)
    {
        player.DetectFloorCollision(value);
    }

    // Metodo per determinare se il giocatore � vicino a un muro
    private bool IsNearWall()
    {
        return player.IsNearWall();
    }
}
