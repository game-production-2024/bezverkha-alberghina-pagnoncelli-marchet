using UnityEngine;

public class PlayerIdleState : PlayerStateBase
{
    public PlayerIdleState(PlayerStateController playerStateController) : base(playerStateController)
    {
    }

    public override void EnterState()
    {
        Debug.Log("Benvenuto nel Idle State");
        player.animator.SetBool("Idle", true);
        player.animator.SetBool("Climb", false);
        player.animator.SetBool("Climb_Idle", false);
    }

    public override void UpdateState()
    {
        // Implementazione del raycasting per rilevare i muri
        DetectWallCollision(IsNearWall());

        if (Input.GetKey(player.rightKey) || Input.GetKey(player.leftKey) && player.isGrounded == true)
        {
            player.SetState(new PlayerMoveState(player));
        }

        if (IsNearWall() && Input.GetKey(player.climbKey)) // Utilizzo di IsNearWall
        {
            player.SetState(new PlayerClimbState(player));
        }
        if (IsNearWall() && Input.GetKey(player.climbKeyAlt)) // Utilizzo di IsNearWall
        {
            player.SetState(new PlayerClimbState(player));
        }

        if (player.jumpsAvailable > 0 && Input.GetKeyDown(player.jumpKey))
        {
            player.SetState(new PlayerJumpState(player));
        }

        if (player.canDash == true && Input.GetKeyDown(player.dashKey))
        {
            player.SetState(new PlayerDashState(player));
        }

        if (Input.GetKeyDown(player.ipdKey) && player.canUseIPD == true)
        {
            player.IPD();
        }
        if (Input.GetKeyDown(player.ipdKeyAlt) && player.canUseIPD == true)
        {
            player.IPD();
        }

        if (Input.GetKeyDown(player.deathKey))
        {
            player.Death();
        }
    }

    public override void FixedUpdateState()
    {
        // Implementazione del raycasting per rilevare i muri anche in FixedUpdate, se necessario
        DetectWallCollision(IsNearWall());
    }

    public override void ExitState()
    {
        player.animator.SetBool("Idle", false);
    }

    // Metodo per rilevare la collisione con il muro
    private void DetectWallCollision(bool value)
    {
        player.DetectWallCollision(value);
    }

    // Metodo per determinare se il giocatore � vicino a un muro
    private bool IsNearWall()
    {
        return player.IsNearWall();
    }
}