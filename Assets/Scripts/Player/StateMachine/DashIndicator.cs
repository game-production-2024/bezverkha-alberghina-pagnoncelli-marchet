using UnityEngine;

public class DashIndicator : MonoBehaviour
{
    private PlayerStateController player;
    private SpriteRenderer spriteRenderer;

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

        spriteRenderer.enabled = false;

        // Trova il componente Player nello stesso GameObject del genitore
        player = GetComponentInParent<PlayerStateController>();

        if (player == null)
        {
            Debug.LogError("DashIndicator non pu� trovare il componente Player nel genitore.");
        }

        // Trova il componente SpriteRenderer in questo GameObject
        spriteRenderer = GetComponent<SpriteRenderer>();

        if (spriteRenderer == null)
        {
            Debug.LogError("DashIndicator non pu� trovare il componente SpriteRenderer.");
        }
    }

    void Update()
    {
        if (player.dashUnlock == false)
        {
            spriteRenderer.enabled = false;
        }
        if (player.dashUnlock == true)
        {
            spriteRenderer.enabled = true;
        }
        else
        {
            return;
        }

        // Controlla se il dash � disponibile e attiva/disattiva il SpriteRenderer di conseguenza
        if (player.canDash && !spriteRenderer.enabled)
        {
            spriteRenderer.enabled = true;
        }
        else if (!player.canDash && spriteRenderer.enabled)
        {
            spriteRenderer.enabled = false;
        }
    }
}
