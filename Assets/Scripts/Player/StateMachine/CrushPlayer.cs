using UnityEngine;

public class CrushPlayer : MonoBehaviour
{
    public GameObject deathParticlePref;
    private PlayerStateController playerStateController;
    private GameObject playerObject;

    private void Awake()
    {
        playerStateController = GetComponentInParent<PlayerStateController>();
        playerObject = playerStateController.gameObject; // Ottieni il game object del player

        if (playerStateController == null)
        {
            Debug.LogError("PlayerStateController non trovato nel genitore!");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((collision.CompareTag("Wall") || collision.CompareTag("MovingPlatform")) && playerStateController.isClimbing)
        {
            // Distruggi il game object del player
            playerObject.gameObject.SetActive(false);

            Debug.Log("Il giocatore � stato distrutto!");
            GameManager.Instance.GameOver(true);

            if (deathParticlePref != null)
            {
                Instantiate(deathParticlePref, collision.transform.position, collision.transform.rotation);
            }
        }
    }
}


