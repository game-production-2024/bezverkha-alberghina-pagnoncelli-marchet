using System.Collections;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerDashState : PlayerStateBase
{
    public PlayerDashState(PlayerStateController playerStateController) : base(playerStateController)
    {
    }

    public override void EnterState()
    {
        Debug.Log("Benvenuto nel Dash State");
        if (player.dashUnlock == false)
        {
            return;
        }

        player.animator.SetTrigger("Dash");
        player.StartCoroutine(Dash());
    }

    public override void UpdateState()
    {
        // Determina il nuovo stato in base alle condizioni
        if (player.IsDashing)
        {
            if (IsNearWall() && Input.GetKey(player.climbKey) && player.StaminaCounter > 0)
            {
                player.SetState(new PlayerClimbState(player));
            }
            if (IsNearWall() && Input.GetKey(player.climbKeyAlt) && player.StaminaCounter > 0)
            {
                player.SetState(new PlayerClimbState(player));
            }
        }
        else
        {
            if (!player.isGrounded)
            {
                player.SetState(new PlayerFallingState(player));
            }
            if (Input.GetKey(player.rightKey) || Input.GetKey(player.leftKey))
            {
                player.SetState(new PlayerMoveState(player));
            }
            if (player.isGrounded && (!Input.GetKey(player.rightKey) || !Input.GetKey(player.leftKey)))
            {
                player.SetState(new PlayerIdleState(player));
            }
            if (Input.GetKeyDown(player.jumpKey))
            {
                player.SetState(new PlayerJumpState(player));
            }   
        }

        if (Input.GetKeyDown(player.ipdKey) && player.canUseIPD == true)
        {
            player.IPD();
        }
        if (Input.GetKeyDown(player.ipdKeyAlt) && player.canUseIPD == true)
        {
            player.IPD();
        }

        if (Input.GetKeyDown(player.deathKey))
        {
            player.Death();
        }
    }

    public override void FixedUpdateState()
    {
    }

    public override void ExitState()
    {
    }

    private IEnumerator Dash()
    {
        if (player.IsDashing)
            yield break;

        AudioManager.Instance.PlaySFX(AudioManager.Instance.dash);
        player.canDash = false;
        player.IsDashing = true;

        // **Disattiva l'associazione alla piattaforma mobile durante il dash**
        player.currentMovingPlatform = null; // Disassocia il giocatore dalla piattaforma mobile

        float startTime = Time.time;
        float dashSpeed = player.dashDistance / player.dashDuration;
        Vector3 dashDirection = player.transform.localRotation.y == 0 ? Vector3.right : Vector3.left;

        // Imposta la velocità iniziale del dash
        player.rb.velocity = dashDirection * dashSpeed;

        while (Time.time < startTime + player.dashDuration)
        {
            // Verifica se il giocatore è morto
            if (!player.gameObject.activeSelf)
            {
                // Se il giocatore è morto, esci dalla coroutine e permetti il dash al respawn
                player.IsDashing = false;
                yield break;
            }

            // Diminuisci gradualmente la velocità del dash
            float elapsed = Time.time - startTime;
            float t = elapsed / player.dashDuration;
            player.rb.velocity = Vector3.Lerp(dashDirection * dashSpeed, Vector3.zero, t);

            // Verifica se il giocatore è entrato in contatto con un muro
            if (IsNearWall())
            {
                player.rb.velocity = Vector2.zero;
                break;
            }

            yield return null;
        }

        player.IsDashing = false;

        // Aspetta che il giocatore tocchi di nuovo il terreno prima di ripristinare il dash
        while (!player.isGrounded)
        {
            player.canDash = false;
            yield return null;
        }

        // **Riattiva l'associazione alla piattaforma mobile**
        if (player.isGrounded && player.currentMovingPlatform != null)
        {
            // Se il giocatore atterra su una piattaforma mobile, ricollega la sua posizione
            player.platformRelativePosition = player.transform.position - player.currentMovingPlatform.position;
        }

        // Ripristina il dash se il giocatore è ancora a terra
        if (player.isGrounded)
        {
            player.canDash = true;
        }

        player.canDash = true;
    }


    // Metodo per determinare se il giocatore è vicino a un muro
    private bool IsNearWall()
    {
        return player.IsNearWall();
    }
}
