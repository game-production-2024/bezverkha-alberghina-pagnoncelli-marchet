using UnityEngine;

public class IpdIndicator : MonoBehaviour
{
    private PlayerStateController player;
    private SpriteRenderer spriteRenderer;

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

        spriteRenderer.enabled = false;

        // Trova il componente Player nello stesso GameObject del genitore
        player = GetComponentInParent<PlayerStateController>();

        if (player == null)
        {
            Debug.LogError("DashIndicator non pu� trovare il componente Player nel genitore.");
        }

        // Trova il componente SpriteRenderer in questo GameObject
        spriteRenderer = GetComponent<SpriteRenderer>();

        if (spriteRenderer == null)
        {
            Debug.LogError("DashIndicator non pu� trovare il componente SpriteRenderer.");
        }
    }

    void Update()
    {
        if (player.ipdUnlock == true)
        {
            spriteRenderer.enabled = true;
        }
        else
        {
            return;
        }

        // Controlla se il dash � disponibile e attiva/disattiva il SpriteRenderer di conseguenza
        if (player.canUseIPD && !spriteRenderer.enabled)
        {
            spriteRenderer.enabled = true;
        }
        else if (!player.canUseIPD && spriteRenderer.enabled)
        {
            spriteRenderer.enabled = false;
        }
    }
}
