using System.Collections;
using UnityEngine;

public class PlayerJumpState : PlayerStateBase
{
    private float originalVelocityX; // Memorizza la velocit� originale sull'asse X

    private bool isMovementLocked;  // Indica se il movimento opposto � bloccato
    private float lockDuration = 0.5f;  // Durata del blocco (mezzo secondo)
    private float lockTimer;  // Timer per gestire la durata del blocco

    public PlayerJumpState(PlayerStateController playerStateController) : base(playerStateController)
    {
    }

    public override void EnterState()
    {
        Debug.Log("Benvenuto nel Jump State");
        player.PrepareForJump();

        if (player.isGrounded == true && Input.GetKeyDown(player.jumpKey))
        {
            Jump();
        }
    }

    public override void UpdateState()
    {
        if (isMovementLocked)
        {
            lockTimer -= Time.deltaTime;
            if (lockTimer <= 0)
            {
                isMovementLocked = false;  // Sblocca il movimento dopo il tempo specificato
            }
        }

        if (player.nearWall && player.staminaCounter > 0 && Input.GetKey(player.climbKey))
        {
            player.RestoreGravity(); // Ripristina la gravit  prima di cambiare stato
            player.SetState(new PlayerClimbState(player));
            return;
        }
        if (player.nearWall && player.staminaCounter > 0 && Input.GetKey(player.climbKeyAlt))
        {
            player.RestoreGravity(); // Ripristina la gravit  prima di cambiare stato
            player.SetState(new PlayerClimbState(player));
            return;
        }

        if (player.canDash == true && Input.GetKeyDown(player.dashKey))
        {
            player.SetState(new PlayerDashState(player));
            return;
        }

        // Aggiungi controllo per il picco del salto
        if (player.rb.velocity.y <= 0)
        {
            player.RestoreGravity(); // Ripristina la gravit  prima di cambiare stato
            player.SetState(new PlayerFallingState(player));
            return;
        }

        if (Input.GetKeyDown(player.ipdKey) && player.canUseIPD == true)
        {
            player.IPD();
        }
        if (Input.GetKeyDown(player.ipdKeyAlt) && player.canUseIPD == true)
        {
            player.IPD();
        }

        if (Input.GetKeyDown(player.deathKey))
        {
            player.Death();
        }
    }

    public override void FixedUpdateState()
    {
        float moveInput = Input.GetAxis("Horizontal");

        // Se il movimento � bloccato, impedisci il movimento nella direzione opposta
        if (isMovementLocked)
        {
            // Se il giocatore � rivolto a destra, impedisci di muoversi a sinistra
            if (!player.isFacingRight && moveInput < 0)
            {
                moveInput = 0;  // Blocca il movimento verso sinistra
            }
            // Se il giocatore � rivolto a sinistra, impedisci di muoversi a destra
            else if (player.isFacingRight && moveInput > 0)
            {
                moveInput = 0;  // Blocca il movimento verso destra
            }
        }

        // Muovi il giocatore orizzontalmente normalmente se non � bloccato
        player.MovePlayer(moveInput);

        if (IsNearWall())
        {
            return;
        }

        // Controlla se il giocatore non sta arrampicando e non sta eseguendo uno scatto
        if (!player.isClimbing && !player.IsDashing)
        {
            // Gestisce la direzione in cui il giocatore sta guardando
            if (Input.GetKeyDown(player.rightKey) || moveInput > 0)
            {
                player.isFacingRight = true;
                player.RotatePlayer(true);
            }
            else if (Input.GetKeyDown(player.leftKey) || moveInput < 0)
            {
                player.isFacingRight = false;
                player.RotatePlayer(false);
            }

            // Muovi il giocatore orizzontalmente
            player.MovePlayer(moveInput);

            // Controlla la collisione con il muro
            player.DetectWallCollision(IsNearWall());
        }
    }

    public override void ExitState()
    {
        player.RestoreGravity(); // Assicura che la gravit  venga ripristinata quando si esce dallo stato
    }

    private void Jump()
    {
        if (player.jumpsAvailable <= 0)
        {
            Debug.LogWarning("Salti esauriti. Non puoi saltare.");
            return;
        }

        if (player.rb == null)
        {
            Debug.LogWarning("Rigidbody2D non trovato. Assicurati che il GameObject abbia un Rigidbody2D.");
            return;
        }

        player.rb.velocity = new Vector2(player.rb.velocity.x, 0f);

        Vector2 jumpDirection = Vector2.up;
        player.rb.AddForce(jumpDirection * player.jumpForce, ForceMode2D.Impulse);

        player.coyoteTimeCounter = 0f; // Resetta il Coyote Time dopo il salto
        player.jumpsAvailable--;
        player.animator.SetTrigger("Jump");


        //Questo codicino fa un codice (Spo)
        //ps: dovreste rendere Instance anche il game manager per non doverlo referenziare in ogni vostro oggetto
        AudioManager.Instance.PlaySFX(AudioManager.Instance.jump);
    }

    public void WallJump()
    {
        // Controlla se il wall jump � in cooldown
        if (player.isWallJumpOnCooldown)
        {
            return;  // Esci dalla funzione se il wall jump � ancora in cooldown
        }

        // Esegui il wall jump
        player.isWallJumping = true;

        // Disabilita la gravit� per consentire il wall jump
        player.rb.gravityScale = player.originalGravityScale;

        // Calcola la direzione del wall jump con un'angolazione di 30 gradi
        float wallJumpAngle = 30f;

        // Blocca il movimento opposto
        isMovementLocked = true;
        lockTimer = lockDuration;

        if (player.isFacingRight)
        {
            // Se il giocatore sta guardando a destra, il wall jump lo spinge a sinistra
            Vector2 wallJumpDirection = Quaternion.Euler(0, 0, wallJumpAngle) * Vector2.left * player.wallJumpHorizontalForce;
            wallJumpDirection += Vector2.up * player.wallJumpVerticalForce;  // Aggiungi forza verso l'alto
            player.rb.velocity = wallJumpDirection.normalized * player.wallJumpForce;

            // Ruota il giocatore a sinistra
            player.RotatePlayer(false);
            player.isFacingRight = false;  // Imposta il giocatore a guardare a sinistra
        }
        else
        {
            // Se il giocatore sta guardando a sinistra, il wall jump lo spinge a destra
            Vector2 wallJumpDirection = Quaternion.Euler(0, 0, -wallJumpAngle) * Vector2.right * player.wallJumpHorizontalForce;
            wallJumpDirection += Vector2.up * player.wallJumpVerticalForce;  // Aggiungi forza verso l'alto
            player.rb.velocity = wallJumpDirection.normalized * player.wallJumpForce;

            // Ruota il giocatore a destra
            player.RotatePlayer(true);
            player.isFacingRight = true;  // Imposta il giocatore a guardare a destra
        }

        // Decrementa il numero di salti disponibili
        player.jumpsAvailable--;
        player.animator.SetTrigger("Jump");

        // Imposta il cooldown del wall jump
        player.isWallJumpOnCooldown = true;
        player.wallJumpCooldownTimer = player.wallJumpCooldownTime;  // Inizia il timer di cooldown
        AudioManager.Instance.PlaySFX(AudioManager.Instance.jump);
    }

    public void ResetAllForces()
    {
        // Imposta la velocit� lineare a zero
        player.rb.velocity = Vector2.zero;

        // Imposta la velocit� angolare a zero
        player.rb.angularVelocity = 0f;

        // Non esiste un metodo equivalente a ResetInertiaTensor per Rigidbody2D
    }
    private bool IsNearWall()
    {
        return player.IsNearWall();
    }

}

