using DG.Tweening;
using UnityEngine;

public class WaveCollider : MonoBehaviour
{
    [SerializeField] private float waveTime = 0.75f;
    [SerializeField] private float endScale = 6f;
    private PlayerStateController player;


    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStateController>();
    }

    private void Update()
    {
        transform.DOScale(endScale, waveTime).SetEase(Ease.Linear);
        
        if (player.ipdActive == false)
        {
            Destroy(gameObject);
        }
    }
}
