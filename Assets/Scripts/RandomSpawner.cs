using UnityEngine;

public class RandomSpawner : MonoBehaviour
{
    public GameObject stonePref;

    public float spawnTimer = 7f;

    private float minX = 0;
    private float maxX = 0;

    private float timer = 0;

    private void Awake()
    {
        timer = spawnTimer;
    }
 
    private void Update()
    {
        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }
        else
        {
            SpawnStone();
            timer = spawnTimer;
        }
    }

    private void SpawnStone()
    {
        minX  = GetComponent<SpriteRenderer>().bounds.min.x; 
        maxX  = GetComponent<SpriteRenderer>().bounds.max.x; 

        Vector2 newPosition = new Vector2(Random.Range(minX, maxX), transform.position.y);

        Instantiate(stonePref, newPosition, Quaternion.identity);
    }
}
